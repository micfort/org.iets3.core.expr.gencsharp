<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4820614e-fa5c-409b-b88f-ae55029d8ba1(org.iets3.core.expr.gencsharp.genplan)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="19" />
    <use id="7ab1a6fa-0a11-4b95-9e48-75f363d6cb00" name="jetbrains.mps.lang.generator.plan" version="1" />
  </languages>
  <imports />
  <registry>
    <language id="7ab1a6fa-0a11-4b95-9e48-75f363d6cb00" name="jetbrains.mps.lang.generator.plan">
      <concept id="1820634577908471803" name="jetbrains.mps.lang.generator.plan.structure.Plan" flags="ng" index="2VgMpV">
        <child id="1820634577908471815" name="steps" index="2VgMA7" />
      </concept>
      <concept id="1820634577908471810" name="jetbrains.mps.lang.generator.plan.structure.Transform" flags="ng" index="2VgMA2">
        <child id="2944629966652439181" name="languages" index="1t_9vn" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="3542851458883438784" name="jetbrains.mps.lang.smodel.structure.LanguageId" flags="nn" index="2V$Bhx">
        <property id="3542851458883439831" name="namespace" index="2V$B1Q" />
        <property id="3542851458883439832" name="languageId" index="2V$B1T" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2VgMpV" id="2Y7ZIgghj2k">
    <property role="TrG5h" value="Csharp_genplan" />
    <node concept="2VgMA2" id="26UovjNCVIx" role="2VgMA7">
      <node concept="2V$Bhx" id="26UovjNCX97" role="1t_9vn">
        <property role="2V$B1T" value="a1f7400d-15d4-44cb-9e88-f2bc4ab8ab0c" />
        <property role="2V$B1Q" value="org.iets3.core.expr.gencsharp.tests" />
      </node>
      <node concept="2V$Bhx" id="26UovjNCX9i" role="1t_9vn">
        <property role="2V$B1T" value="d5adc5cd-212e-4ddd-a0b1-eefbc86ff2e5" />
        <property role="2V$B1Q" value="org.iets3.core.expr.gencsharp.toplevel" />
      </node>
      <node concept="2V$Bhx" id="26UovjNCX8I" role="1t_9vn">
        <property role="2V$B1T" value="61f84ed8-4dc7-4a56-b3ff-3b5752e642b2" />
        <property role="2V$B1Q" value="org.iets3.core.expr.gencsharp.base" />
      </node>
      <node concept="2V$Bhx" id="26UovjNCX8Y" role="1t_9vn">
        <property role="2V$B1T" value="07db36c0-316f-4566-9d1f-3c79dcbd5acd" />
        <property role="2V$B1Q" value="org.iets3.core.expr.gencsharp.simpleTypes" />
      </node>
      <node concept="2V$Bhx" id="26UovjNCX8R" role="1t_9vn">
        <property role="2V$B1T" value="d56ead1b-93f8-4be1-9ced-00efad349d71" />
        <property role="2V$B1Q" value="org.iets3.core.expr.gencsharp.contracts" />
      </node>
    </node>
  </node>
</model>

