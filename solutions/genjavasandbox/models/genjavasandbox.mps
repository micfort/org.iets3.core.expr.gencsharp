<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:dd235968-5a15-4d50-b1b6-3bbd6946960f(genjavasandbox)">
  <persistence version="9" />
  <languages>
    <devkit ref="c4e521ab-b605-4ef9-a7c3-68075da058f0(org.iets3.core.expr.core.devkit)" />
    <devkit ref="b2a65b84-7ec9-404f-8602-f16394bb1d98(org.iets3.core.expr.stateful.devkit)" />
    <devkit ref="ce1cf8e2-ad23-4a29-b20d-ca13a97e194f(org.iets3.core.expr.advanced.devkit)" />
    <devkit ref="ffc660b2-672c-4f91-9291-8426ed4e58de(org.iets3.core.expr.genjava.advanced.devkit)" />
  </languages>
  <imports />
  <registry>
    <language id="2f7e2e35-6e74-4c43-9fa5-2465d68f5996" name="org.iets3.core.expr.collections">
      <concept id="8872269265520081293" name="org.iets3.core.expr.collections.structure.SetLiteral" flags="ng" index="2TO1xI">
        <child id="8872269265520081294" name="elements" index="2TO1xH" />
      </concept>
      <concept id="7554398283340020764" name="org.iets3.core.expr.collections.structure.OneArgCollectionOp" flags="ng" index="3iAY4E">
        <child id="7554398283340020765" name="arg" index="3iAY4F" />
      </concept>
      <concept id="7554398283339850572" name="org.iets3.core.expr.collections.structure.FirstOp" flags="ng" index="3iB7TU" />
      <concept id="7554398283339759319" name="org.iets3.core.expr.collections.structure.ListLiteral" flags="ng" index="3iBYfx">
        <child id="7554398283339759320" name="elements" index="3iBYfI" />
      </concept>
      <concept id="8448265401163111273" name="org.iets3.core.expr.collections.structure.KeyValuePair" flags="ng" index="1DGDZQ">
        <child id="8448265401163111276" name="val" index="1DGDZN" />
        <child id="8448265401163111274" name="key" index="1DGDZP" />
      </concept>
      <concept id="9097157441620016186" name="org.iets3.core.expr.collections.structure.ForeachOp" flags="ng" index="3NG6h4" />
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="2390066428848651932" name="org.iets3.core.expr.base.structure.BangOp" flags="ng" index="wdKpt" />
      <concept id="7089558164908491660" name="org.iets3.core.expr.base.structure.EmptyExpression" flags="ng" index="2zH6wq" />
      <concept id="7089558164905593724" name="org.iets3.core.expr.base.structure.IOptionallyTyped" flags="ng" index="2zM23E">
        <child id="7089558164905593725" name="type" index="2zM23F" />
      </concept>
      <concept id="7071042522334260296" name="org.iets3.core.expr.base.structure.ITyped" flags="ng" index="2_iKZX">
        <child id="8811147530085329321" name="type" index="2S399n" />
      </concept>
      <concept id="5115872837156802409" name="org.iets3.core.expr.base.structure.UnaryExpression" flags="ng" index="30czhk">
        <child id="5115872837156802411" name="expr" index="30czhm" />
      </concept>
      <concept id="5115872837156687890" name="org.iets3.core.expr.base.structure.LessExpression" flags="ng" index="30d6GJ" />
      <concept id="5115872837156578546" name="org.iets3.core.expr.base.structure.PlusExpression" flags="ng" index="30dDZf" />
      <concept id="5115872837156576277" name="org.iets3.core.expr.base.structure.BinaryExpression" flags="ng" index="30dEsC">
        <child id="5115872837156576280" name="right" index="30dEs_" />
        <child id="5115872837156576278" name="left" index="30dEsF" />
      </concept>
      <concept id="7849560302565679722" name="org.iets3.core.expr.base.structure.IfExpression" flags="ng" index="39w5ZF">
        <child id="7849560302565679723" name="condition" index="39w5ZE" />
        <child id="7849560302565679725" name="thenPart" index="39w5ZG" />
      </concept>
      <concept id="9002563722476995145" name="org.iets3.core.expr.base.structure.DotExpression" flags="ng" index="1QScDb">
        <child id="9002563722476995147" name="target" index="1QScD9" />
      </concept>
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="5115872837157054169" name="org.iets3.core.expr.simpleTypes.structure.IntegerType" flags="ng" index="30bXR$" />
      <concept id="5115872837157054170" name="org.iets3.core.expr.simpleTypes.structure.NumberLiteral" flags="ng" index="30bXRB">
        <property id="5115872837157054173" name="value" index="30bXRw" />
      </concept>
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="8293738266727773586" name="org.iets3.core.expr.toplevel.structure.GroupByOp" flags="ng" index="23hzag" />
      <concept id="8293738266729562040" name="org.iets3.core.expr.toplevel.structure.GroupMembersTarget" flags="ng" index="23oZyU" />
      <concept id="7089558164906249676" name="org.iets3.core.expr.toplevel.structure.Constant" flags="ng" index="2zPypq">
        <child id="7089558164906249715" name="value" index="2zPyp_" />
      </concept>
      <concept id="543569365051789113" name="org.iets3.core.expr.toplevel.structure.ConstantRef" flags="ng" index="_emDc">
        <reference id="543569365051789114" name="constant" index="_emDf" />
      </concept>
      <concept id="543569365052765011" name="org.iets3.core.expr.toplevel.structure.EmptyToplevelContent" flags="ng" index="_ixoA" />
      <concept id="543569365052711055" name="org.iets3.core.expr.toplevel.structure.Library" flags="ng" index="_iOnU">
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="8811147530085329320" name="org.iets3.core.expr.toplevel.structure.RecordLiteral" flags="ng" index="2S399m">
        <child id="8811147530085329323" name="memberValues" index="2S399l" />
      </concept>
      <concept id="602952467877559919" name="org.iets3.core.expr.toplevel.structure.IRecordDeclaration" flags="ng" index="S5Q1W">
        <child id="602952467877562565" name="members" index="S5Trm" />
      </concept>
      <concept id="8811147530084018370" name="org.iets3.core.expr.toplevel.structure.RecordType" flags="ng" index="2Ss9cW">
        <reference id="8811147530084018371" name="record" index="2Ss9cX" />
      </concept>
      <concept id="8811147530084018361" name="org.iets3.core.expr.toplevel.structure.RecordMember" flags="ng" index="2Ss9d7" />
      <concept id="8811147530084018358" name="org.iets3.core.expr.toplevel.structure.RecordDeclaration" flags="ng" index="2Ss9d8" />
      <concept id="4790956042240148643" name="org.iets3.core.expr.toplevel.structure.Function" flags="ng" index="1aga60" />
    </language>
    <language id="fbba5118-5fc6-49ff-9c3b-0b4469830440" name="org.iets3.core.expr.mutable">
      <concept id="4255172619709548950" name="org.iets3.core.expr.mutable.structure.BoxType" flags="ng" index="3sNe5_">
        <child id="4255172619709548951" name="baseType" index="3sNe5$" />
      </concept>
      <concept id="4255172619711277794" name="org.iets3.core.expr.mutable.structure.BoxUpdateTarget" flags="ng" index="3sPC8h">
        <child id="4255172619711277798" name="value" index="3sPC8l" />
      </concept>
      <concept id="4255172619710841704" name="org.iets3.core.expr.mutable.structure.BoxValueTarget" flags="ng" index="3sQ2Ir" />
      <concept id="4255172619710740510" name="org.iets3.core.expr.mutable.structure.BoxExpression" flags="ng" index="3sRH3H">
        <child id="4255172619710740514" name="value" index="3sRH3h" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="f3eafff0-30d2-46d6-9150-f0f3b880ce27" name="org.iets3.core.expr.path">
      <concept id="7814222126786013807" name="org.iets3.core.expr.path.structure.PathElement" flags="ng" index="3o_JK">
        <reference id="7814222126786013810" name="member" index="3o_JH" />
      </concept>
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="5096753237482793710" name="org.iets3.core.expr.lambda.structure.ReadModifyEffectTag" flags="ng" index="2lgajY" />
      <concept id="4790956042240983401" name="org.iets3.core.expr.lambda.structure.BlockExpression" flags="ng" index="1aduha">
        <child id="4790956042240983402" name="expressions" index="1aduh9" />
      </concept>
      <concept id="4790956042241105569" name="org.iets3.core.expr.lambda.structure.ValRef" flags="ng" index="1adzI2">
        <reference id="4790956042241106533" name="val" index="1adwt6" />
      </concept>
      <concept id="4790956042241053102" name="org.iets3.core.expr.lambda.structure.ValExpression" flags="ng" index="1adJid">
        <child id="4790956042241053105" name="expr" index="1adJii" />
      </concept>
      <concept id="4790956042240100911" name="org.iets3.core.expr.lambda.structure.IFunctionLike" flags="ng" index="1ahQWc">
        <child id="3880322347437217307" name="effect" index="28QfE6" />
        <child id="4790956042240100950" name="body" index="1ahQXP" />
      </concept>
      <concept id="7554398283340741814" name="org.iets3.core.expr.lambda.structure.ShortLambdaExpression" flags="ng" index="3izI60">
        <child id="7554398283340741815" name="expression" index="3izI61" />
      </concept>
      <concept id="7554398283340826520" name="org.iets3.core.expr.lambda.structure.ShortLambdaItExpression" flags="ng" index="3izPEI" />
    </language>
  </registry>
  <node concept="_iOnU" id="kFBfAaoGCo">
    <property role="TrG5h" value="test" />
    <node concept="2zPypq" id="kFBfAar2yf" role="_iOnB">
      <property role="TrG5h" value="a" />
      <node concept="3sRH3H" id="kFBfAasSoc" role="2zPyp_">
        <node concept="30bXRB" id="kFBfAasSor" role="3sRH3h">
          <property role="30bXRw" value="10" />
        </node>
      </node>
      <node concept="3sNe5_" id="19T$JBHTko" role="2zM23F">
        <node concept="30bXR$" id="19T$JBHTtr" role="3sNe5$" />
      </node>
    </node>
    <node concept="1aga60" id="kFBfAasSrC" role="_iOnB">
      <property role="TrG5h" value="b" />
      <node concept="1aduha" id="kFBfAasSKK" role="1ahQXP">
        <node concept="1QScDb" id="kFBfAasSPe" role="1aduh9">
          <node concept="3sPC8h" id="kFBfAasSRi" role="1QScD9">
            <node concept="30bXRB" id="kFBfAasSYm" role="3sPC8l">
              <property role="30bXRw" value="10" />
            </node>
          </node>
          <node concept="_emDc" id="kFBfAasSN6" role="30czhm">
            <ref role="_emDf" node="kFBfAar2yf" resolve="a" />
          </node>
        </node>
        <node concept="30bXRB" id="kFBfAasT5n" role="1aduh9">
          <property role="30bXRw" value="10" />
        </node>
      </node>
      <node concept="2lgajY" id="kFBfAasT96" role="28QfE6" />
    </node>
    <node concept="1aga60" id="19T$JBHOiU" role="_iOnB">
      <property role="TrG5h" value="c" />
      <node concept="2lgajY" id="19T$JBHSzY" role="28QfE6" />
      <node concept="1aduha" id="19T$JBJ6B7" role="1ahQXP">
        <node concept="1adJid" id="19T$JBJ70B" role="1aduh9">
          <property role="TrG5h" value="l" />
          <node concept="3iBYfx" id="19T$JBJ7l7" role="1adJii">
            <node concept="30bXRB" id="19T$JBJ7l8" role="3iBYfI">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="19T$JBJ7l9" role="3iBYfI">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="19T$JBJ7la" role="3iBYfI">
              <property role="30bXRw" value="3" />
            </node>
          </node>
        </node>
        <node concept="1QScDb" id="19T$JBHOtr" role="1aduh9">
          <node concept="3NG6h4" id="19T$JBHOGC" role="1QScD9">
            <node concept="3izI60" id="19T$JBHOGD" role="3iAY4F">
              <node concept="1aduha" id="19T$JBHPU1" role="3izI61">
                <node concept="1adJid" id="19T$JBHQ1A" role="1aduh9">
                  <property role="TrG5h" value="tmp" />
                  <node concept="30dDZf" id="19T$JBHQxu" role="1adJii">
                    <node concept="1QScDb" id="19T$JBHQOj" role="30dEs_">
                      <node concept="3sQ2Ir" id="19T$JBHQX9" role="1QScD9" />
                      <node concept="_emDc" id="19T$JBHQDO" role="30czhm">
                        <ref role="_emDf" node="kFBfAar2yf" resolve="a" />
                      </node>
                    </node>
                    <node concept="3izPEI" id="19T$JBHQo8" role="30dEsF" />
                  </node>
                </node>
                <node concept="39w5ZF" id="54c1Cfm1EUE" role="1aduh9">
                  <node concept="30d6GJ" id="54c1Cfm1Fst" role="39w5ZE">
                    <node concept="30bXRB" id="54c1Cfm1FBX" role="30dEs_">
                      <property role="30bXRw" value="10" />
                    </node>
                    <node concept="1adzI2" id="54c1Cfm1FgU" role="30dEsF">
                      <ref role="1adwt6" node="19T$JBHQ1A" resolve="tmp" />
                    </node>
                  </node>
                  <node concept="1aduha" id="54c1Cfm1IlJ" role="39w5ZG">
                    <node concept="1QScDb" id="54c1Cfm1Iww" role="1aduh9">
                      <node concept="3sPC8h" id="54c1Cfm1IHa" role="1QScD9">
                        <node concept="1adzI2" id="54c1Cfm1IS8" role="3sPC8l">
                          <ref role="1adwt6" node="19T$JBHQ1A" resolve="tmp" />
                        </node>
                      </node>
                      <node concept="_emDc" id="54c1Cfm1Iwb" role="30czhm">
                        <ref role="_emDf" node="kFBfAar2yf" resolve="a" />
                      </node>
                    </node>
                    <node concept="1QScDb" id="54c1Cfm1Jqo" role="1aduh9">
                      <node concept="3sPC8h" id="54c1Cfm1JBA" role="1QScD9">
                        <node concept="30dDZf" id="54c1Cfm1JZH" role="3sPC8l">
                          <node concept="30bXRB" id="54c1Cfm1JZU" role="30dEs_">
                            <property role="30bXRw" value="1" />
                          </node>
                          <node concept="1adzI2" id="54c1Cfm1JN4" role="30dEsF">
                            <ref role="1adwt6" node="19T$JBHQ1A" resolve="tmp" />
                          </node>
                        </node>
                      </node>
                      <node concept="_emDc" id="54c1Cfm1JdV" role="30czhm">
                        <ref role="_emDf" node="kFBfAar2yf" resolve="a" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2zH6wq" id="54c1Cfm1HDY" role="1aduh9" />
              </node>
            </node>
          </node>
          <node concept="1adzI2" id="19T$JBJ7$E" role="30czhm">
            <ref role="1adwt6" node="19T$JBJ70B" resolve="l" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="2hT7BxDgb1O" role="_iOnB">
      <property role="TrG5h" value="e" />
      <node concept="1DGDZQ" id="2hT7BxDgb3z" role="1ahQXP">
        <node concept="30bXRB" id="2hT7BxDgb3Y" role="1DGDZN">
          <property role="30bXRw" value="3" />
        </node>
        <node concept="30bXRB" id="2hT7BxDgb2O" role="1DGDZP">
          <property role="30bXRw" value="1" />
        </node>
      </node>
    </node>
    <node concept="_ixoA" id="26UovjNpvI6" role="_iOnB" />
    <node concept="2Ss9d8" id="26UovjNpwng" role="_iOnB">
      <property role="TrG5h" value="groupByTest" />
      <node concept="2Ss9d7" id="26UovjNpwot" role="S5Trm">
        <property role="TrG5h" value="group" />
        <node concept="30bXR$" id="26UovjNpwoz" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="26UovjNpwoY" role="S5Trm">
        <property role="TrG5h" value="data" />
        <node concept="30bXR$" id="26UovjNpwp6" role="2S399n" />
      </node>
    </node>
    <node concept="1aga60" id="26UovjNpvJR" role="_iOnB">
      <property role="TrG5h" value="g" />
      <node concept="1QScDb" id="26UovjNpPHO" role="1ahQXP">
        <node concept="23oZyU" id="26UovjNpPOu" role="1QScD9" />
        <node concept="wdKpt" id="26UovjNpPBh" role="30czhm">
          <node concept="1QScDb" id="26UovjNpP29" role="30czhm">
            <node concept="3iB7TU" id="26UovjNpP6Z" role="1QScD9" />
            <node concept="1QScDb" id="26UovjNpw_b" role="30czhm">
              <node concept="23hzag" id="26UovjNpwDj" role="1QScD9">
                <node concept="3izI60" id="26UovjNpwDk" role="3iAY4F">
                  <node concept="1QScDb" id="26UovjNpwH7" role="3izI61">
                    <node concept="3o_JK" id="26UovjNpwJe" role="1QScD9">
                      <ref role="3o_JH" node="26UovjNpwot" resolve="group" />
                    </node>
                    <node concept="3izPEI" id="26UovjNpwDm" role="30czhm" />
                  </node>
                </node>
              </node>
              <node concept="3iBYfx" id="26UovjNpvL6" role="30czhm">
                <node concept="2S399m" id="26UovjNpwpv" role="3iBYfI">
                  <node concept="2Ss9cW" id="26UovjNpwpJ" role="2S399n">
                    <ref role="2Ss9cX" node="26UovjNpwng" resolve="groupByTest" />
                  </node>
                  <node concept="30bXRB" id="26UovjNpws$" role="2S399l">
                    <property role="30bXRw" value="1" />
                  </node>
                  <node concept="30bXRB" id="26UovjNpwvC" role="2S399l">
                    <property role="30bXRw" value="2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="26UovjNpPVb" role="_iOnB">
      <property role="TrG5h" value="h" />
      <node concept="1QScDb" id="26UovjNpRab" role="1ahQXP">
        <node concept="23hzag" id="26UovjNpRjq" role="1QScD9">
          <node concept="3izI60" id="26UovjNpRjr" role="3iAY4F">
            <node concept="1QScDb" id="26UovjNpRtU" role="3izI61">
              <node concept="3o_JK" id="26UovjNpRBa" role="1QScD9">
                <ref role="3o_JH" node="26UovjNpwot" resolve="group" />
              </node>
              <node concept="3izPEI" id="26UovjNpRjt" role="30czhm" />
            </node>
          </node>
        </node>
        <node concept="2TO1xI" id="26UovjNpQ_7" role="30czhm">
          <node concept="2S399m" id="26UovjNpR1O" role="2TO1xH">
            <node concept="2Ss9cW" id="26UovjNpR1P" role="2S399n">
              <ref role="2Ss9cX" node="26UovjNpwng" resolve="groupByTest" />
            </node>
            <node concept="30bXRB" id="26UovjNpR1Q" role="2S399l">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="26UovjNpR1R" role="2S399l">
              <property role="30bXRw" value="2" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

