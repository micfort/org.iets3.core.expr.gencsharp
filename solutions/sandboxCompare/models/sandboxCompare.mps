<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c5cb569b-2594-4caa-b3dc-8213f185741f(sandboxCompare)">
  <persistence version="9" />
  <languages>
    <devkit ref="c4e521ab-b605-4ef9-a7c3-68075da058f0(org.iets3.core.expr.core.devkit)" />
    <devkit ref="da9c5738-b245-4e38-b52f-e6ee76cadd7f(org.iets3.core.expr.genjava.core.devkit)" />
  </languages>
  <imports />
  <registry>
    <language id="7b68d745-a7b8-48b9-bd9c-05c0f8725a35" name="org.iets3.core.base">
      <concept id="229512757698888199" name="org.iets3.core.base.structure.IOptionallyNamed" flags="ng" index="pfQq$">
        <child id="229512757698888936" name="optionalName" index="pfQ1b" />
      </concept>
      <concept id="229512757698888202" name="org.iets3.core.base.structure.OptionalNameSpecifier" flags="ng" index="pfQqD">
        <property id="229512757698888203" name="optionalName" index="pfQqC" />
      </concept>
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="7971844778466793051" name="org.iets3.core.expr.base.structure.AltOption" flags="ng" index="2fGnzd">
        <child id="7971844778466793072" name="then" index="2fGnzA" />
        <child id="7971844778466793070" name="when" index="2fGnzS" />
      </concept>
      <concept id="7971844778466793028" name="org.iets3.core.expr.base.structure.AlternativesExpression" flags="ng" index="2fGnzi">
        <child id="7971844778466793162" name="alternatives" index="2fGnxs" />
      </concept>
      <concept id="7089558164909884363" name="org.iets3.core.expr.base.structure.TryErrorClause" flags="ng" index="2zzUxt">
        <child id="7089558164909884398" name="expr" index="2zzUxS" />
        <child id="7089558164910923907" name="errorLiteral" index="2zBOGl" />
      </concept>
      <concept id="7089558164910719190" name="org.iets3.core.expr.base.structure.SuccessValueExpr" flags="ng" index="2zAAH0">
        <reference id="7089558164910719191" name="try" index="2zAAH1" />
      </concept>
      <concept id="6481804410367607231" name="org.iets3.core.expr.base.structure.TrySuccessClause" flags="ng" index="2YtBXV">
        <child id="6481804410367607232" name="expr" index="2YtBW4" />
      </concept>
      <concept id="6481804410367226920" name="org.iets3.core.expr.base.structure.TryExpression" flags="ng" index="2Yz4FG">
        <child id="7089558164909885123" name="errorClauses" index="2zzUPl" />
        <child id="6481804410367607310" name="successClause" index="2YtBNa" />
        <child id="6481804410367226948" name="expr" index="2Yz4E0" />
      </concept>
      <concept id="5115872837157187871" name="org.iets3.core.expr.base.structure.ParensExpression" flags="ng" index="30bsCy">
        <child id="5115872837157187954" name="expr" index="30bsDf" />
      </concept>
      <concept id="5115872837156687890" name="org.iets3.core.expr.base.structure.LessExpression" flags="ng" index="30d6GJ" />
      <concept id="5115872837156652603" name="org.iets3.core.expr.base.structure.DivExpression" flags="ng" index="30dvO6" />
      <concept id="5115872837156578546" name="org.iets3.core.expr.base.structure.PlusExpression" flags="ng" index="30dDZf" />
      <concept id="5115872837156576277" name="org.iets3.core.expr.base.structure.BinaryExpression" flags="ng" index="30dEsC">
        <child id="5115872837156576280" name="right" index="30dEs_" />
        <child id="5115872837156576278" name="left" index="30dEsF" />
      </concept>
      <concept id="1919538606559981270" name="org.iets3.core.expr.base.structure.ErrorLiteral" flags="ng" index="1i17NB" />
      <concept id="1919538606560895472" name="org.iets3.core.expr.base.structure.ErrorExpression" flags="ng" index="1i5Bf1">
        <child id="1919538606560895473" name="error" index="1i5Bf0" />
      </concept>
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="5115872837157252552" name="org.iets3.core.expr.simpleTypes.structure.StringLiteral" flags="ng" index="30bdrP">
        <property id="5115872837157252555" name="value" index="30bdrQ" />
      </concept>
      <concept id="5115872837157054170" name="org.iets3.core.expr.simpleTypes.structure.NumberLiteral" flags="ng" index="30bXRB">
        <property id="5115872837157054173" name="value" index="30bXRw" />
      </concept>
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="7061117989422575313" name="org.iets3.core.expr.toplevel.structure.EnumLiteral" flags="ng" index="5mgYR" />
      <concept id="7061117989422575278" name="org.iets3.core.expr.toplevel.structure.EnumDeclaration" flags="ng" index="5mgZ8">
        <child id="7061117989422575348" name="literals" index="5mgYi" />
      </concept>
      <concept id="7061117989422577349" name="org.iets3.core.expr.toplevel.structure.EnumLiteralRef" flags="ng" index="5mhuz">
        <reference id="7061117989422577417" name="literal" index="5mhpJ" />
      </concept>
      <concept id="543569365052765011" name="org.iets3.core.expr.toplevel.structure.EmptyToplevelContent" flags="ng" index="_ixoA" />
      <concept id="543569365052711055" name="org.iets3.core.expr.toplevel.structure.Library" flags="ng" index="_iOnU">
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="4790956042240570348" name="org.iets3.core.expr.toplevel.structure.FunctionCall" flags="ng" index="1af_rf" />
      <concept id="4790956042240148643" name="org.iets3.core.expr.toplevel.structure.Function" flags="ng" index="1aga60" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="4790956042240522396" name="org.iets3.core.expr.lambda.structure.IFunctionCall" flags="ng" index="1afhQZ">
        <reference id="4790956042240522408" name="function" index="1afhQb" />
      </concept>
      <concept id="4790956042240100911" name="org.iets3.core.expr.lambda.structure.IFunctionLike" flags="ng" index="1ahQWc">
        <child id="4790956042240100950" name="body" index="1ahQXP" />
      </concept>
    </language>
  </registry>
  <node concept="_iOnU" id="26UovjNBtBV">
    <property role="TrG5h" value="sometest" />
    <node concept="1aga60" id="1qqzbvY8Ktu" role="_iOnB">
      <property role="TrG5h" value="foo" />
      <node concept="30dDZf" id="1qqzbvY8KET" role="1ahQXP">
        <node concept="30dvO6" id="1qqzbvY8KEU" role="30dEsF">
          <node concept="30bXRB" id="1qqzbvY8KEV" role="30dEsF">
            <property role="30bXRw" value="10" />
          </node>
          <node concept="30bXRB" id="1qqzbvY8KEW" role="30dEs_">
            <property role="30bXRw" value="3" />
          </node>
        </node>
        <node concept="30dvO6" id="1qqzbvY8KEX" role="30dEs_">
          <node concept="30dvO6" id="1qqzbvY8KEY" role="30dEsF">
            <node concept="30bXRB" id="1qqzbvY8KEZ" role="30dEsF">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="1qqzbvY8KF0" role="30dEs_">
              <property role="30bXRw" value="2" />
            </node>
          </node>
          <node concept="30bXRB" id="1qqzbvY8KHf" role="30dEs_">
            <property role="30bXRw" value="3" />
          </node>
        </node>
      </node>
    </node>
    <node concept="5mgZ8" id="26UovjNwXPr" role="_iOnB">
      <property role="TrG5h" value="color" />
      <node concept="5mgYR" id="26UovjNwXPC" role="5mgYi">
        <property role="TrG5h" value="red" />
      </node>
      <node concept="5mgYR" id="26UovjNwXQo" role="5mgYi">
        <property role="TrG5h" value="blue" />
      </node>
      <node concept="5mgYR" id="26UovjNwXQv" role="5mgYi">
        <property role="TrG5h" value="green" />
      </node>
    </node>
    <node concept="1aga60" id="26UovjNwXQK" role="_iOnB">
      <property role="TrG5h" value="choose" />
      <node concept="5mhuz" id="26UovjNwXR1" role="1ahQXP">
        <ref role="5mhpJ" node="26UovjNwXPC" resolve="red" />
      </node>
    </node>
    <node concept="1aga60" id="4ejLsrQtxB_" role="_iOnB">
      <property role="TrG5h" value="u" />
      <node concept="30bsCy" id="4ejLsrQtyx9" role="1ahQXP">
        <node concept="2fGnzi" id="4ejLsrQtxVG" role="30bsDf">
          <node concept="2fGnzd" id="4ejLsrQtxVH" role="2fGnxs">
            <node concept="30d6GJ" id="4ejLsrQtxVJ" role="2fGnzS">
              <node concept="30bXRB" id="4ejLsrQtxVK" role="30dEs_">
                <property role="30bXRw" value="2" />
              </node>
              <node concept="30bXRB" id="4ejLsrQtxVL" role="30dEsF">
                <property role="30bXRw" value="1" />
              </node>
            </node>
            <node concept="30bdrP" id="54c1CflVfi3" role="2fGnzA">
              <property role="30bdrQ" value="bla" />
            </node>
          </node>
          <node concept="2fGnzd" id="4ejLsrQtxVM" role="2fGnxs">
            <node concept="30d6GJ" id="4ejLsrQtxVN" role="2fGnzS">
              <node concept="30bXRB" id="4ejLsrQtxVO" role="30dEs_">
                <property role="30bXRw" value="1" />
              </node>
              <node concept="30bXRB" id="4ejLsrQtxVP" role="30dEsF">
                <property role="30bXRw" value="2" />
              </node>
            </node>
            <node concept="1i5Bf1" id="4ejLsrQtxVQ" role="2fGnzA">
              <node concept="1i17NB" id="4ejLsrQtxVR" role="1i5Bf0">
                <property role="TrG5h" value="blub" />
              </node>
            </node>
          </node>
          <node concept="2fGnzd" id="4ejLsrQtxVS" role="2fGnxs">
            <node concept="30d6GJ" id="4ejLsrQtxVT" role="2fGnzS">
              <node concept="30bXRB" id="4ejLsrQtxVU" role="30dEs_">
                <property role="30bXRw" value="3" />
              </node>
              <node concept="30bXRB" id="4ejLsrQtxVV" role="30dEsF">
                <property role="30bXRw" value="1" />
              </node>
            </node>
            <node concept="1i5Bf1" id="4ejLsrQtxVW" role="2fGnzA">
              <node concept="1i17NB" id="4ejLsrQtxVX" role="1i5Bf0">
                <property role="TrG5h" value="yes" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="6c02FvVKl5h" role="_iOnB">
      <property role="TrG5h" value="v" />
      <node concept="2Yz4FG" id="3tcPhB34le2" role="1ahQXP">
        <node concept="2YtBXV" id="3tcPhB34le3" role="2YtBNa">
          <node concept="2zAAH0" id="3tcPhB34zm3" role="2YtBW4">
            <ref role="2zAAH1" node="3tcPhB34le2" resolve="op" />
          </node>
        </node>
        <node concept="1af_rf" id="3tcPhB34lie" role="2Yz4E0">
          <ref role="1afhQb" node="4ejLsrQtxB_" resolve="u" />
        </node>
        <node concept="2zzUxt" id="3tcPhB34n2S" role="2zzUPl">
          <node concept="1i17NB" id="3tcPhB34n2R" role="2zBOGl">
            <property role="TrG5h" value="blub" />
          </node>
          <node concept="30bdrP" id="3tcPhB34nm1" role="2zzUxS">
            <property role="30bdrQ" value="bla2" />
          </node>
        </node>
        <node concept="2zzUxt" id="3tcPhB34q1f" role="2zzUPl">
          <node concept="30bdrP" id="3tcPhB34q6o" role="2zzUxS">
            <property role="30bdrQ" value="yes" />
          </node>
        </node>
        <node concept="pfQqD" id="3tcPhB34z0I" role="pfQ1b">
          <property role="pfQqC" value="op" />
        </node>
      </node>
    </node>
    <node concept="_ixoA" id="26UovjNLQ3z" role="_iOnB" />
    <node concept="_ixoA" id="26UovjNBZSA" role="_iOnB" />
  </node>
</model>

