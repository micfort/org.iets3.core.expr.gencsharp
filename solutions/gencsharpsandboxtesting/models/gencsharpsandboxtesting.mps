<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f4f0f45d-342a-4e04-9b50-7a3aee344662(gencsharpsandboxtesting)">
  <persistence version="9" />
  <languages>
    <use id="2022a471-10ba-4431-ba5d-622df898f3c6" name="org.iets3.core.expr.testExecution" version="0" />
    <use id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test" version="5" />
    <devkit ref="c4e521ab-b605-4ef9-a7c3-68075da058f0(org.iets3.core.expr.core.devkit)" />
    <devkit ref="79d31620-2d34-40bc-8b87-b764e10a007a(org.iets3.core.expr.gencsharp.devkit)" />
  </languages>
  <imports>
    <import index="1n7c" ref="r:6239d1b0-5938-4eed-b7a2-61692aee558c(gencsharpsandbox)" />
    <import index="g9p5" ref="r:88f22b8b-30cf-42e2-ae08-ddc85066a7b8(test.in.expr.os.strings@tests)" />
  </imports>
  <registry>
    <language id="8585453e-6bfb-4d80-98de-b16074f1d86c" name="jetbrains.mps.lang.test">
      <concept id="5097124989038916362" name="jetbrains.mps.lang.test.structure.TestInfo" flags="ng" index="2XOHcx">
        <property id="5097124989038916363" name="projectPath" index="2XOHcw" />
      </concept>
    </language>
    <language id="2f7e2e35-6e74-4c43-9fa5-2465d68f5996" name="org.iets3.core.expr.collections">
      <concept id="7554398283339749509" name="org.iets3.core.expr.collections.structure.CollectionType" flags="ng" index="3iBWmN">
        <child id="7554398283339749510" name="baseType" index="3iBWmK" />
      </concept>
      <concept id="7554398283339759319" name="org.iets3.core.expr.collections.structure.ListLiteral" flags="ng" index="3iBYfx">
        <child id="7554398283339759320" name="elements" index="3iBYfI" />
      </concept>
      <concept id="7554398283339757344" name="org.iets3.core.expr.collections.structure.ListType" flags="ng" index="3iBYCm" />
    </language>
    <language id="7b68d745-a7b8-48b9-bd9c-05c0f8725a35" name="org.iets3.core.base">
      <concept id="229512757698888199" name="org.iets3.core.base.structure.IOptionallyNamed" flags="ng" index="pfQq$">
        <child id="229512757698888936" name="optionalName" index="pfQ1b" />
      </concept>
      <concept id="229512757698888202" name="org.iets3.core.base.structure.OptionalNameSpecifier" flags="ng" index="pfQqD">
        <property id="229512757698888203" name="optionalName" index="pfQqC" />
      </concept>
    </language>
    <language id="cfaa4966-b7d5-4b69-b66a-309a6e1a7290" name="org.iets3.core.expr.base">
      <concept id="411710798114972602" name="org.iets3.core.expr.base.structure.FailExpr" flags="ng" index="qoPdK">
        <child id="7275867333401405429" name="type" index="4IRkZ" />
        <child id="411710798114972606" name="message" index="qoPdO" />
      </concept>
      <concept id="7089558164905593724" name="org.iets3.core.expr.base.structure.IOptionallyTyped" flags="ng" index="2zM23E">
        <child id="7089558164905593725" name="type" index="2zM23F" />
      </concept>
      <concept id="7071042522334260296" name="org.iets3.core.expr.base.structure.ITyped" flags="ng" index="2_iKZX">
        <child id="8811147530085329321" name="type" index="2S399n" />
      </concept>
      <concept id="2807135271608265973" name="org.iets3.core.expr.base.structure.NoneLiteral" flags="ng" index="UmHTt" />
      <concept id="2807135271607939856" name="org.iets3.core.expr.base.structure.OptionType" flags="ng" index="Uns6S">
        <child id="2807135271607939857" name="baseType" index="Uns6T" />
      </concept>
      <concept id="5115872837156578546" name="org.iets3.core.expr.base.structure.PlusExpression" flags="ng" index="30dDZf" />
      <concept id="5115872837156576277" name="org.iets3.core.expr.base.structure.BinaryExpression" flags="ng" index="30dEsC">
        <child id="5115872837156576280" name="right" index="30dEs_" />
        <child id="5115872837156576278" name="left" index="30dEsF" />
      </concept>
    </language>
    <language id="d441fba0-f46b-43cd-b723-dad7b65da615" name="org.iets3.core.expr.tests">
      <concept id="4988624180052598016" name="org.iets3.core.expr.tests.structure.RealEqualsTestOp" flags="ng" index="2cNFD2">
        <property id="4988624180052918199" name="decimals" index="2cKlzP" />
      </concept>
      <concept id="543569365052056273" name="org.iets3.core.expr.tests.structure.EqualsTestOp" flags="ng" index="_fku$" />
      <concept id="543569365052056263" name="org.iets3.core.expr.tests.structure.TestCase" flags="ng" index="_fkuM">
        <child id="543569365052056368" name="items" index="_fkp5" />
      </concept>
      <concept id="543569365052056266" name="org.iets3.core.expr.tests.structure.AssertTestItem" flags="ng" index="_fkuZ">
        <child id="543569365052056302" name="op" index="_fkur" />
        <child id="543569365052056269" name="expected" index="_fkuS" />
        <child id="543569365052056267" name="actual" index="_fkuY" />
      </concept>
      <concept id="543569365052711055" name="org.iets3.core.expr.tests.structure.TestSuite" flags="ng" index="_iOnU">
        <property id="7740953487931061385" name="referenceOnlyLocalStuff" index="1XBH2A" />
        <child id="543569365052711058" name="contents" index="_iOnB" />
      </concept>
      <concept id="8255774724000586868" name="org.iets3.core.expr.tests.structure.ReportTestItem" flags="ng" index="2F9BGE">
        <child id="543569365052056267" name="actual" index="_fkuZ" />
      </concept>
      <concept id="3822903164827733126" name="org.iets3.core.expr.tests.structure.AssertOptionTestItem" flags="ng" index="1biUaE">
        <property id="3822903164827733176" name="what" index="1biUak" />
        <child id="543569365052056267" name="actual" index="_fkv0" />
      </concept>
      <concept id="1925389232535425850" name="org.iets3.core.expr.tests.structure.AndMatcher" flags="ng" index="1h6CxO">
        <child id="1925389232535425913" name="right" index="1h6CwR" />
        <child id="1925389232535425911" name="left" index="1h6CwT" />
      </concept>
      <concept id="1927432956093755937" name="org.iets3.core.expr.tests.structure.NotEqualsTestOp" flags="ng" index="3uTIKI" />
      <concept id="6723982381150106591" name="org.iets3.core.expr.tests.structure.ContainsString" flags="ng" index="3_fT66">
        <child id="6723982381150106625" name="text" index="3_fTpo" />
      </concept>
      <concept id="6723982381143750170" name="org.iets3.core.expr.tests.structure.AssertThatTestItem" flags="ng" index="3_nDh3">
        <child id="6723982381143776833" name="matcher" index="3_nNKo" />
        <child id="6723982381143776835" name="value" index="3_nNKq" />
      </concept>
      <concept id="6723982381145448848" name="org.iets3.core.expr.tests.structure.IsValidRecord" flags="ng" index="3_u8f9" />
      <concept id="6723982381145831383" name="org.iets3.core.expr.tests.structure.IsInvalid" flags="ng" index="3_vHme" />
    </language>
    <language id="6b277d9a-d52d-416f-a209-1919bd737f50" name="org.iets3.core.expr.simpleTypes">
      <concept id="5115872837157252552" name="org.iets3.core.expr.simpleTypes.structure.StringLiteral" flags="ng" index="30bdrP">
        <property id="5115872837157252555" name="value" index="30bdrQ" />
      </concept>
      <concept id="5115872837157054169" name="org.iets3.core.expr.simpleTypes.structure.IntegerType" flags="ng" index="30bXR$" />
      <concept id="5115872837157054170" name="org.iets3.core.expr.simpleTypes.structure.NumberLiteral" flags="ng" index="30bXRB">
        <property id="5115872837157054173" name="value" index="30bXRw" />
      </concept>
    </language>
    <language id="71934284-d7d1-45ee-a054-8c072591085f" name="org.iets3.core.expr.toplevel">
      <concept id="543569365052765011" name="org.iets3.core.expr.toplevel.structure.EmptyToplevelContent" flags="ng" index="_ixoA" />
      <concept id="543569365052711055" name="org.iets3.core.expr.toplevel.structure.Library" flags="ng" index="_iOnV">
        <child id="543569365052711058" name="contents" index="_iOnC" />
      </concept>
      <concept id="8811147530085329320" name="org.iets3.core.expr.toplevel.structure.RecordLiteral" flags="ng" index="2S399m">
        <child id="8811147530085329323" name="memberValues" index="2S399l" />
      </concept>
      <concept id="602952467877559919" name="org.iets3.core.expr.toplevel.structure.IRecordDeclaration" flags="ng" index="S5Q1W">
        <child id="602952467877562565" name="members" index="S5Trm" />
      </concept>
      <concept id="8811147530084018370" name="org.iets3.core.expr.toplevel.structure.RecordType" flags="ng" index="2Ss9cW">
        <reference id="8811147530084018371" name="record" index="2Ss9cX" />
      </concept>
      <concept id="8811147530084018361" name="org.iets3.core.expr.toplevel.structure.RecordMember" flags="ng" index="2Ss9d7" />
      <concept id="8811147530084018358" name="org.iets3.core.expr.toplevel.structure.RecordDeclaration" flags="ng" index="2Ss9d8" />
      <concept id="4790956042240570348" name="org.iets3.core.expr.toplevel.structure.FunctionCall" flags="ng" index="1af_rf" />
      <concept id="4790956042240148643" name="org.iets3.core.expr.toplevel.structure.Function" flags="ng" index="1aga60" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="2022a471-10ba-4431-ba5d-622df898f3c6" name="org.iets3.core.expr.testExecution">
      <concept id="4473287864570292399" name="org.iets3.core.expr.testExecution.structure.TestExecutionConfig" flags="ng" index="3ZOQsN">
        <child id="4473287864570320758" name="executionMode" index="3ZOXzE" />
      </concept>
      <concept id="4473287864570320810" name="org.iets3.core.expr.testExecution.structure.GeneratorExecutionMode" flags="ng" index="3ZOXwQ" />
    </language>
    <language id="9464fa06-5ab9-409b-9274-64ab29588457" name="org.iets3.core.expr.lambda">
      <concept id="4790956042240407469" name="org.iets3.core.expr.lambda.structure.ArgRef" flags="ng" index="1afdae">
        <reference id="4790956042240460422" name="arg" index="1afue_" />
      </concept>
      <concept id="4790956042240522396" name="org.iets3.core.expr.lambda.structure.IFunctionCall" flags="ng" index="1afhQZ">
        <reference id="4790956042240522408" name="function" index="1afhQb" />
        <child id="4790956042240522406" name="args" index="1afhQ5" />
      </concept>
      <concept id="4790956042240100911" name="org.iets3.core.expr.lambda.structure.IFunctionLike" flags="ng" index="1ahQWc">
        <child id="4790956042240100927" name="args" index="1ahQWs" />
        <child id="4790956042240100950" name="body" index="1ahQXP" />
      </concept>
      <concept id="4790956042240100929" name="org.iets3.core.expr.lambda.structure.FunctionArgument" flags="ng" index="1ahQXy" />
      <concept id="7554398283340318473" name="org.iets3.core.expr.lambda.structure.IArgument" flags="ng" index="3ix9CZ">
        <child id="7554398283340318476" name="type" index="3ix9CU" />
      </concept>
    </language>
  </registry>
  <node concept="_iOnU" id="26UovjNXWjV">
    <property role="TrG5h" value="MainTest" />
    <property role="1XBH2A" value="true" />
    <node concept="_fkuM" id="26UovjNY9x$" role="_iOnB">
      <property role="TrG5h" value="assertsTest" />
      <node concept="_fkuZ" id="26UovjNY9xA" role="_fkp5">
        <node concept="_fku$" id="26UovjNY9xB" role="_fkur" />
        <node concept="30bXRB" id="26UovjNY9xC" role="_fkuS">
          <property role="30bXRw" value="12" />
        </node>
        <node concept="30dDZf" id="26UovjNY9xD" role="_fkuY">
          <node concept="30bXRB" id="26UovjNY9xE" role="30dEsF">
            <property role="30bXRw" value="10" />
          </node>
          <node concept="30bXRB" id="26UovjNY9xF" role="30dEs_">
            <property role="30bXRw" value="2" />
          </node>
        </node>
      </node>
      <node concept="_fkuZ" id="26UovjNY9xG" role="_fkp5">
        <node concept="3uTIKI" id="26UovjNYXtU" role="_fkur" />
        <node concept="30bXRB" id="26UovjNY9xI" role="_fkuS">
          <property role="30bXRw" value="14" />
        </node>
        <node concept="30dDZf" id="26UovjNY9xJ" role="_fkuY">
          <node concept="30bXRB" id="26UovjNY9xK" role="30dEsF">
            <property role="30bXRw" value="10" />
          </node>
          <node concept="30bXRB" id="26UovjNY9xL" role="30dEs_">
            <property role="30bXRw" value="2" />
          </node>
        </node>
      </node>
      <node concept="_fkuZ" id="26UovjNZ4Rv" role="_fkp5">
        <node concept="2cNFD2" id="26UovjNZ4SS" role="_fkur">
          <property role="2cKlzP" value="2" />
        </node>
        <node concept="30bXRB" id="26UovjNZ4SE" role="_fkuY">
          <property role="30bXRw" value="1.000" />
        </node>
        <node concept="30bXRB" id="26UovjNZ4T4" role="_fkuS">
          <property role="30bXRw" value="1.002" />
        </node>
      </node>
      <node concept="_fkuZ" id="26UovjNYznI" role="_fkp5">
        <node concept="30bXRB" id="26UovjNYzp6" role="_fkuS">
          <property role="30bXRw" value="123" />
        </node>
        <node concept="30bXRB" id="26UovjNZLbD" role="_fkuY">
          <property role="30bXRw" value="124" />
        </node>
        <node concept="3uTIKI" id="26UovjNYXtY" role="_fkur" />
      </node>
      <node concept="_fkuZ" id="26UovjNYFiu" role="_fkp5">
        <node concept="_fku$" id="26UovjNYUDv" role="_fkur" />
        <node concept="3iBYfx" id="26UovjNYFj6" role="_fkuY">
          <node concept="3iBYfx" id="26UovjNYFEW" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYFFs" role="3iBYfI">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="26UovjNYFGj" role="3iBYfI">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="26UovjNYFWp" role="3iBYfI">
              <property role="30bXRw" value="3" />
            </node>
          </node>
          <node concept="3iBYfx" id="26UovjNYGa3" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYGbP" role="3iBYfI">
              <property role="30bXRw" value="4" />
            </node>
            <node concept="30bXRB" id="26UovjNYGPk" role="3iBYfI">
              <property role="30bXRw" value="5" />
            </node>
            <node concept="30bXRB" id="26UovjNYHtd" role="3iBYfI">
              <property role="30bXRw" value="6" />
            </node>
          </node>
        </node>
        <node concept="3iBYfx" id="26UovjNYHG3" role="_fkuS">
          <node concept="3iBYfx" id="26UovjNYHG4" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYHG5" role="3iBYfI">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="26UovjNYHG6" role="3iBYfI">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="26UovjNYHG7" role="3iBYfI">
              <property role="30bXRw" value="3" />
            </node>
          </node>
          <node concept="3iBYfx" id="26UovjNYHG8" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYHG9" role="3iBYfI">
              <property role="30bXRw" value="4" />
            </node>
            <node concept="30bXRB" id="26UovjNYHGa" role="3iBYfI">
              <property role="30bXRw" value="5" />
            </node>
            <node concept="30bXRB" id="26UovjNYW5B" role="3iBYfI">
              <property role="30bXRw" value="6" />
            </node>
          </node>
        </node>
      </node>
      <node concept="_fkuZ" id="26UovjNYTWM" role="_fkp5">
        <node concept="3uTIKI" id="26UovjNYTWN" role="_fkur" />
        <node concept="3iBYfx" id="26UovjNYTWO" role="_fkuY">
          <node concept="3iBYfx" id="26UovjNYTWP" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYTWQ" role="3iBYfI">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="26UovjNYTWR" role="3iBYfI">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="26UovjNYTWS" role="3iBYfI">
              <property role="30bXRw" value="3" />
            </node>
          </node>
          <node concept="3iBYfx" id="26UovjNYTWT" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYTWU" role="3iBYfI">
              <property role="30bXRw" value="4" />
            </node>
            <node concept="30bXRB" id="26UovjNYTWV" role="3iBYfI">
              <property role="30bXRw" value="5" />
            </node>
            <node concept="30bXRB" id="26UovjNYTWW" role="3iBYfI">
              <property role="30bXRw" value="6" />
            </node>
          </node>
        </node>
        <node concept="3iBYfx" id="26UovjNYTWX" role="_fkuS">
          <node concept="3iBYfx" id="26UovjNYTWY" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYTWZ" role="3iBYfI">
              <property role="30bXRw" value="1" />
            </node>
            <node concept="30bXRB" id="26UovjNYTX0" role="3iBYfI">
              <property role="30bXRw" value="2" />
            </node>
            <node concept="30bXRB" id="26UovjNYTX1" role="3iBYfI">
              <property role="30bXRw" value="3" />
            </node>
          </node>
          <node concept="3iBYfx" id="26UovjNYTX2" role="3iBYfI">
            <node concept="30bXRB" id="26UovjNYTX3" role="3iBYfI">
              <property role="30bXRw" value="4" />
            </node>
            <node concept="30bXRB" id="26UovjNYTX4" role="3iBYfI">
              <property role="30bXRw" value="5" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1aga60" id="26UovjO0Rzq" role="_iOnB">
      <property role="TrG5h" value="ReturnNone" />
      <node concept="Uns6S" id="26UovjO0SC$" role="2zM23F">
        <node concept="3iBYCm" id="26UovjO0SEa" role="Uns6T">
          <node concept="30bXR$" id="26UovjO0SFU" role="3iBWmK" />
        </node>
      </node>
      <node concept="UmHTt" id="26UovjO0SHV" role="1ahQXP" />
    </node>
    <node concept="1aga60" id="26UovjO0SIB" role="_iOnB">
      <property role="TrG5h" value="ReturnSome" />
      <node concept="Uns6S" id="26UovjO0SIC" role="2zM23F">
        <node concept="3iBYCm" id="26UovjO0SID" role="Uns6T">
          <node concept="30bXR$" id="26UovjO0SIE" role="3iBWmK" />
        </node>
      </node>
      <node concept="3iBYfx" id="26UovjO0SLI" role="1ahQXP">
        <node concept="30bXRB" id="26UovjO0SMd" role="3iBYfI">
          <property role="30bXRw" value="1" />
        </node>
        <node concept="30bXRB" id="26UovjO0SMo" role="3iBYfI">
          <property role="30bXRw" value="2" />
        </node>
        <node concept="30bXRB" id="26UovjO0SM_" role="3iBYfI">
          <property role="30bXRw" value="3" />
        </node>
      </node>
    </node>
    <node concept="1aga60" id="7W7a84sLQZB" role="_iOnB">
      <property role="TrG5h" value="ReturnSome2" />
      <node concept="30bXRB" id="7W7a84sLR3w" role="1ahQXP">
        <property role="30bXRw" value="1" />
      </node>
      <node concept="Uns6S" id="7W7a84sLR2P" role="2zM23F">
        <node concept="30bXR$" id="7W7a84sLR3a" role="Uns6T" />
      </node>
    </node>
    <node concept="_fkuM" id="26UovjO0v1V" role="_iOnB">
      <property role="TrG5h" value="assertOpts" />
      <node concept="1biUaE" id="26UovjO0v2V" role="_fkp5">
        <node concept="1af_rf" id="26UovjO0SPR" role="_fkv0">
          <ref role="1afhQb" node="7W7a84sLQZB" resolve="ReturnSome2" />
        </node>
      </node>
      <node concept="1biUaE" id="26UovjO0v3k" role="_fkp5">
        <property role="1biUak" value="3kdFyLYhwMG/none" />
        <node concept="1af_rf" id="26UovjO0SQa" role="_fkv0">
          <ref role="1afhQb" node="26UovjO0Rzq" resolve="ReturnNone" />
        </node>
      </node>
      <node concept="1biUaE" id="26UovjO0v3J" role="_fkp5">
        <property role="1biUak" value="3kdFyLYhwML/coll" />
        <node concept="1af_rf" id="26UovjO0SQw" role="_fkv0">
          <ref role="1afhQb" node="26UovjO0SIB" resolve="ReturnSome" />
        </node>
      </node>
    </node>
    <node concept="2Ss9d8" id="7W7a84sLpsa" role="_iOnB">
      <property role="TrG5h" value="SomeRecord" />
      <node concept="2Ss9d7" id="7W7a84sLptT" role="S5Trm">
        <property role="TrG5h" value="value1" />
        <node concept="30bXR$" id="7W7a84sLptZ" role="2S399n" />
      </node>
      <node concept="2Ss9d7" id="7W7a84sLpua" role="S5Trm">
        <property role="TrG5h" value="value2" />
        <node concept="30bXR$" id="7W7a84sLpub" role="2S399n" />
      </node>
    </node>
    <node concept="1aga60" id="7W7a84sM3YB" role="_iOnB">
      <property role="TrG5h" value="ReturnNoRecord" />
      <node concept="Uns6S" id="7W7a84sM3YC" role="2zM23F">
        <node concept="2Ss9cW" id="7W7a84sM43M" role="Uns6T">
          <ref role="2Ss9cX" node="7W7a84sLpsa" resolve="SomeRecord" />
        </node>
      </node>
      <node concept="UmHTt" id="7W7a84sM3YF" role="1ahQXP" />
    </node>
    <node concept="_ixoA" id="7W7a84sM3V$" role="_iOnB" />
    <node concept="1aga60" id="7W7a84sLK_b" role="_iOnB">
      <property role="TrG5h" value="ReturnIntWithFail" />
      <node concept="qoPdK" id="7W7a84sLKC7" role="1ahQXP">
        <node concept="30bdrP" id="7W7a84sLKCn" role="qoPdO">
          <property role="30bdrQ" value="some reason" />
        </node>
        <node concept="30bXR$" id="7W7a84sLKDJ" role="4IRkZ" />
      </node>
    </node>
    <node concept="1aga60" id="7W7a84sMQ0r" role="_iOnB">
      <property role="TrG5h" value="ReturnIntWithoutFail" />
      <node concept="30bXRB" id="7W7a84sMQ4W" role="1ahQXP">
        <property role="30bXRw" value="10" />
      </node>
    </node>
    <node concept="_fkuM" id="7W7a84sLpeQ" role="_iOnB">
      <property role="TrG5h" value="assertsThat" />
      <node concept="3_nDh3" id="7W7a84sLpga" role="_fkp5">
        <node concept="30bdrP" id="7W7a84sLpiG" role="3_nNKq">
          <property role="30bdrQ" value="hello" />
        </node>
        <node concept="3_fT66" id="7W7a84sLphI" role="3_nNKo">
          <node concept="30bdrP" id="7W7a84sLpjW" role="3_fTpo">
            <property role="30bdrQ" value="lo" />
          </node>
        </node>
      </node>
      <node concept="3_nDh3" id="7W7a84sLpm1" role="_fkp5">
        <node concept="2S399m" id="7W7a84sLpmr" role="3_nNKq">
          <node concept="30bXRB" id="7W7a84sLpv7" role="2S399l">
            <property role="30bXRw" value="10" />
          </node>
          <node concept="30bXRB" id="7W7a84sLpwl" role="2S399l">
            <property role="30bXRw" value="20" />
          </node>
          <node concept="2Ss9cW" id="7W7a84sLpuw" role="2S399n">
            <ref role="2Ss9cX" node="7W7a84sLpsa" resolve="SomeRecord" />
          </node>
        </node>
        <node concept="3_u8f9" id="7W7a84sLpmh" role="3_nNKo" />
      </node>
      <node concept="3_nDh3" id="7W7a84sLpyZ" role="_fkp5">
        <node concept="30bdrP" id="7W7a84sLpD4" role="3_nNKq">
          <property role="30bdrQ" value="hello" />
        </node>
        <node concept="1h6CxO" id="7W7a84sLp$p" role="3_nNKo">
          <node concept="3_fT66" id="7W7a84sLp$D" role="1h6CwT">
            <node concept="30bdrP" id="7W7a84sLp_M" role="3_fTpo">
              <property role="30bdrQ" value="el" />
            </node>
          </node>
          <node concept="3_fT66" id="7W7a84sLp$W" role="1h6CwR">
            <node concept="30bdrP" id="7W7a84sLpBo" role="3_fTpo">
              <property role="30bdrQ" value="lo" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3_nDh3" id="7W7a84sLpFR" role="_fkp5">
        <node concept="1af_rf" id="7W7a84sLKFi" role="3_nNKq">
          <ref role="1afhQb" node="7W7a84sLK_b" resolve="ReturnIntWithFail" />
        </node>
        <node concept="3_vHme" id="7W7a84sLpHo" role="3_nNKo" />
      </node>
    </node>
    <node concept="_fkuM" id="7W7a84sNhLU" role="_iOnB">
      <property role="TrG5h" value="report" />
      <node concept="2F9BGE" id="7W7a84sNhP2" role="_fkp5">
        <node concept="30bXRB" id="7W7a84sNhPr" role="_fkuZ">
          <property role="30bXRw" value="10" />
        </node>
      </node>
      <node concept="2F9BGE" id="7W7a84sNr3i" role="_fkp5">
        <node concept="30bXRB" id="7W7a84sNr3j" role="_fkuZ">
          <property role="30bXRw" value="10" />
        </node>
        <node concept="pfQqD" id="7W7a84sNr3C" role="pfQ1b">
          <property role="pfQqC" value="named" />
        </node>
      </node>
    </node>
  </node>
  <node concept="_iOnV" id="26UovjO0Rol">
    <property role="TrG5h" value="SomeLibrary" />
    <node concept="1aga60" id="26UovjO0Ron" role="_iOnC">
      <property role="TrG5h" value="AddTwo" />
      <node concept="1ahQXy" id="26UovjO0RoP" role="1ahQWs">
        <property role="TrG5h" value="value" />
        <node concept="30bXR$" id="26UovjO0Rp7" role="3ix9CU" />
      </node>
      <node concept="30dDZf" id="26UovjO0Rp_" role="1ahQXP">
        <node concept="30bXRB" id="26UovjO0RpW" role="30dEs_">
          <property role="30bXRw" value="2" />
        </node>
        <node concept="1afdae" id="26UovjO0Rpp" role="30dEsF">
          <ref role="1afue_" node="26UovjO0RoP" resolve="value" />
        </node>
      </node>
    </node>
  </node>
  <node concept="_iOnU" id="26UovjO1qzH">
    <property role="TrG5h" value="ExternalTest" />
    <node concept="_fkuM" id="26UovjO1qzI" role="_iOnB">
      <property role="TrG5h" value="testInModel" />
      <node concept="_fkuZ" id="26UovjO1qzN" role="_fkp5">
        <node concept="_fku$" id="26UovjO1qzO" role="_fkur" />
        <node concept="1af_rf" id="26UovjO1qzZ" role="_fkuY">
          <ref role="1afhQb" node="26UovjO0Ron" resolve="AddTwo" />
          <node concept="30bXRB" id="26UovjO1q$X" role="1afhQ5">
            <property role="30bXRw" value="2" />
          </node>
        </node>
        <node concept="30bXRB" id="26UovjO1q_Z" role="_fkuS">
          <property role="30bXRw" value="4" />
        </node>
      </node>
    </node>
  </node>
  <node concept="3ZOQsN" id="2VImHGx_va7">
    <property role="TrG5h" value="hello" />
    <node concept="3ZOXwQ" id="2VImHGx_va9" role="3ZOXzE" />
  </node>
  <node concept="2XOHcx" id="WHC6UIRLe6">
    <property role="2XOHcw" value="${project_home}" />
  </node>
</model>

