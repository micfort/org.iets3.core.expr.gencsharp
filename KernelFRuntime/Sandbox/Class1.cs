﻿using System.Collections.Immutable;
using System.Diagnostics;
using gencsharpsandbox.blub;
using KernelFRuntime;
using NUnit.Framework;

namespace Sandbox;

public static class Class1
{
    public static void test()
    {
        ImmutableList<int> list = ImmutableList.Create<int>(1, 2, 3);
        ImmutableList<ImmutableList<int>> list2 = ImmutableList.Create(ImmutableList.Create<int>(1, 2, 3), ImmutableList.Create<int>(1, 2, 3), ImmutableList.Create<int>(1, 2, 3));
        ImmutableHashSet<int> set = ImmutableHashSet.Create<int>(1, 2, 3);
        var dictBuilder = ImmutableDictionary.CreateBuilder<int, int>();
        dictBuilder.Add(1, 1);
        dictBuilder.Add(2, 2);
        dictBuilder.Add(3, 3);
        IImmutableDictionary<int, int> dict = dictBuilder.ToImmutable();

        var a = list.Select(it => new AnonymousKernelFRecord(new Dictionary<string, object?>() { { "bla", it }, { "bla2", it * 2 } })).ToImmutableList();
        var b = list.Select<int, (int bla, int bla2)>(it => (it, it*2)).ToImmutableList();
        var c = list.FProject<int, (int bla, int bla2)>(it => (it, it*2));

        var d = Enum.GetValues<color>().ToImmutableList();

        var e = new test2(1, 2, 3) with { a = 10, };
    }

    public static readonly int someName = 13;

public enum color
{
    bla,
    blub,
    bblubblu,
}

public record test2(int a, int b, int c);

public static test2 ToValue(this color self)
{
    return self switch
    {
        color.bla => new test2(1, 2, 3),
        color.blub => new test2(1, 2, 3),
        color.bblubblu => new test2(1, 2, 3),
        _ => throw new ArgumentOutOfRangeException(nameof(self), self, null)
    };
}
public static color ColorByValue(test2 self)
{
    if (self == new test2(1, 2, 3)) return color.bla;
    if (self == new test2(2, 2, 3)) return color.blub;
    if (self == new test2(3, 2, 3)) return color.bblubblu;
    throw new ArgumentOutOfRangeException(nameof(self), self, null);
}
}