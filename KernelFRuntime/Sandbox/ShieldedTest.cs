using System.Diagnostics.Tracing;
using System.Transactions;
using Shielded;

namespace Sandbox;

public static class ShieldedTest
{
    public static readonly Shielded<int> A = new Shielded<int>(10);
    public static readonly AutoResetEvent resetEvent = new(false);
    public static readonly AutoResetEvent resetEventBack = new(false);

    public static void Test()
    {
        var partner = Task.Factory.StartNew(TestPartner);
        Shield.InTransaction(() =>
        {
            Console.Out.WriteLine($"{A.Value}");
            resetEvent.Set();
            resetEventBack.WaitOne();
            Console.Out.WriteLine($"{A.Value}");
        });
        Console.Out.WriteLine($"{A.Value}");
    }

    public static void TestPartner()
    {
        resetEvent.WaitOne();
        Shield.InTransaction(() =>
        {
            A.Value = A.Value+1;
        });
        resetEventBack.Set();
    }
}