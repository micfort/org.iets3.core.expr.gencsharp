using System;
using System.Collections.Immutable;
using KernelFRuntime;
using Xunit;
using Xunit.Abstractions;

namespace SandBoxTesting;

public class UnitTest1
{
private readonly ITestOutputHelper output;

public UnitTest1(ITestOutputHelper output)
{
    this.output = output;
}

    
    [Fact]
    public void Test1()
    {
        // Assert.Equal("", "");
        // Assert.Equal(1.0, 1.03, 1);
        // Assert.Equal(ImmutableList.Create(ImmutableList.Create(1, 2, 3), ImmutableList.Create(4, 5, 6)),
        //     ImmutableList.Create(ImmutableList.Create(1, 2, 3), ImmutableList.Create(4, 5, 6)));
        // Assert.NotEqual(ImmutableList.Create(ImmutableList.Create(1, 2, 3), ImmutableList.Create(4, 5, 6)),
        //     ImmutableList.Create(ImmutableList.Create(1, 2, 3), ImmutableList.Create(4, 5)));
        //
        // Assert.True(((int?)123) is not null);
        // Assert.True(((int?)null) is null);

        var a = new test(1, 2);
        var b = a.GetType();
        output.WriteLine($"{a.GetType()}");
        output.WriteLine($"{b}");

    }

    public record test(int a, int b);
}