using System.Collections.Immutable;

namespace KernelFRuntime;

public class AnonymousKernelFRecord : IKernelFRecord
{
    public AnonymousKernelFRecord(Dictionary<string, object?> values)
    {
        Values = values.ToImmutableDictionary();
    }

    public ImmutableDictionary<string, object?> Values { get; }

    public object? this[string index] => Values[index];


    #region Equality members

    public bool Equals(AnonymousKernelFRecord? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        var selfKeys = Values.Keys.ToImmutableHashSet();
        var otherKeys = other.Values.Keys.ToImmutableHashSet();
        if (selfKeys.SymmetricExcept(otherKeys).Count != 0) return false; //make sure that both key sets are exactly the same
        foreach (var key in selfKeys)
        {
            var selfValue = Values[key];
            var otherValue = other.Values[key];
            if (selfValue is null && otherValue is null) continue;
            if (selfValue is null) return false;
            if (!selfValue.Equals(otherValue)) return false;
        }

        return true;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((AnonymousKernelFRecord)obj);
    }

    public override int GetHashCode()
    {
        var hc = new HashCode();
        foreach (var value in Values.Values) hc.Add(value);
        return hc.ToHashCode();
    }

    public static bool operator ==(AnonymousKernelFRecord? left, AnonymousKernelFRecord? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(AnonymousKernelFRecord? left, AnonymousKernelFRecord? right)
    {
        return !Equals(left, right);
    }

    #endregion
}