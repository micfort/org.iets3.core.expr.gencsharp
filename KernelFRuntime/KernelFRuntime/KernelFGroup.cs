using System.Collections.Immutable;

namespace KernelFRuntime;

public record KernelFGroup<TKey, TElement>(TKey Key, IImmutableList<TElement> Members);