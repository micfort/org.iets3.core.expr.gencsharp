﻿using System.Collections.Immutable;
using System.Diagnostics;

namespace KernelFRuntime;

public static class AH
{
    public static bool InRange(int expr, int lowerBound, bool lowerExcluded, int upperBound, bool upperExcluded)
    {
        if (lowerExcluded)
        {
            if (expr <= lowerBound) return false;
        }
        else if (expr < lowerBound) return false;

        if (upperExcluded)
        {
            if (expr >= upperBound) return false;
        }
        else if (expr > upperBound) return false;
        
        return true;
    }

    public static bool Iff(bool lhs, bool rhs) => (lhs && rhs) || (!lhs && !rhs);

    public static int FFirstIndex<T>(this IEnumerable<T> self, T element) => self.Cast<T?>().Select((x, i) => (x, i)).FirstOrDefault(x => x.Item1?.Equals(element) ?? false, (default(T), -1)).Item2;

    public static bool FIsEmpty<T>(this IEnumerable<T> self) => !self.Any();
    public static bool FIsNotEmpty<T>(this IEnumerable<T> self) => self.Any();


    public static ImmutableList<T> FFirstN<T>(this ImmutableList<T> self, int count) => self.GetRange(0, count);
    public static ImmutableList<T> FLastN<T>(this ImmutableList<T> self, int count) => self.GetRange(self.Count-count, count);
    public static ImmutableList<T> FTail<T>(this ImmutableList<T> self) => self.GetRange(1, self.Count-1);
    public static ImmutableList<T> FDistinct<T>(this ImmutableList<T> self) => self.Distinct().ToImmutableList();
    public static ImmutableList<T> FReverse<T>(this ImmutableList<T> self) => self.Reverse().ToImmutableList();
    public static ImmutableList<U> FMap<T, U>(this ImmutableList<T> self, Func<T, int, U> converter) => self.Select(converter).ToImmutableList();
    public static ImmutableHashSet<U> FMap<T, U>(this ImmutableHashSet<T> self, Func<T, int, U> converter) => self.Select(converter).ToImmutableHashSet();
    public static ImmutableList<T> FWhere<T>(this ImmutableList<T> self, Func<T, bool> selector) => self.Where(selector).ToImmutableList();
    public static ImmutableHashSet<T> FWhere<T>(this ImmutableHashSet<T> self, Func<T, bool> selector) => self.Where(selector).ToImmutableHashSet();
    public static ImmutableList<T> FFlatten<T>(this ImmutableList<ImmutableList<T>> self) => self.SelectMany(x => x).ToImmutableList();
    public static ImmutableList<T> FFlatten<T>(this ImmutableList<ImmutableHashSet<T>> self) => self.SelectMany(x => x).ToImmutableList();
    public static ImmutableList<T> FFlatten<T>(this ImmutableHashSet<ImmutableList<T>> self) => self.SelectMany(x => x).ToImmutableList();
    public static ImmutableList<T> FFlatten<T>(this ImmutableHashSet<ImmutableHashSet<T>> self) => self.SelectMany(x => x).ToImmutableList();

    public static ImmutableList<KernelFGroup<TKey, TElement>> FGroupBy<TKey, TElement>(this IEnumerable<TElement> self, Func<TElement, TKey> keySelector) =>
        self.GroupBy(keySelector).Select(x => new KernelFGroup<TKey, TElement>(x.Key, x.ToImmutableList())).ToImmutableList();

    public static ImmutableList<TResult> FProject<TSource, TResult>(this IEnumerable<TSource> self, Func<TSource, TResult> projector) => self.Select(projector).ToImmutableList();

    public static IEnumerable<T> FUnpack<T>(this IEnumerable<T?> self) where T: struct => self.Where(x => x != null).Cast<T>();
    public static IEnumerable<T> FUnpack<T>(this IEnumerable<T?> self) where T: class => self.Where(x => x != null).Cast<T>();

    public static string FJoinString(this IEnumerable<string> self, string separator) => string.Join(separator, self);
    public static string FTerminateString(this IEnumerable<string> self, string separator) => string.Join(separator, self) + separator;

    public static ImmutableList<TKey> FKeys<TKey, TValue>(this ImmutableDictionary<TKey, TValue> self) => self.Keys.ToImmutableList();
    public static ImmutableList<TValue> FValues<TKey, TValue>(this ImmutableDictionary<TKey, TValue> self) => self.Values.ToImmutableList();
    
    public static ImmutableHashSet<T> FDiff<T>(this ImmutableHashSet<T> self, IEnumerable<T> other) => self.Except(other).ToImmutableHashSet();

    public static ImmutableList<T> FListPick<T>(this ImmutableList<T> self, IEnumerable<int> indices) => indices.Select(self.ElementAt).ToImmutableList(); 

    public static Func<T, TReturn2> Compose<T, TReturn1, TReturn2>(this Func<TReturn1, TReturn2> func1, Func<T, TReturn1> func2) => x => func1(func2(x));

    public static bool Assert(bool condition)
    {
        Debug.Assert(condition);
        return true;
    }

    public static T Error<T>(string errorName) => throw new KernelFErrorException(errorName);

    public static TAccumulate Aggregate<TSource, TAccumulate>(
        this IEnumerable<TSource> source,
        TAccumulate seed,
        Func<TAccumulate, TSource, int, TAccumulate> func)
    {
        return source
            .Select((content, i) => (content, i))
            .Aggregate(seed, (accumulate, source) => func(accumulate, source.content, source.i));
    }

}

public class KernelFTransactionException : Exception
{
    
}

public class KernelFErrorException : Exception
{
    public string ErrorLiteral { get; }

    public KernelFErrorException(string errorLiteral)
    {
        ErrorLiteral = errorLiteral;
    }
}

public class KernelFAlternativesException : Exception
{
}

public class KernelFFailException : Exception
{
    public KernelFFailException(string? message) : base(message)
    {
    }
}