<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e9f070f0-acdc-4844-bee2-488bcede7e64(org.iets3.core.expr.gencsharp.__spreferences.TestExecutionPreferences)">
  <persistence version="9" />
  <languages>
    <use id="2022a471-10ba-4431-ba5d-622df898f3c6" name="org.iets3.core.expr.testExecution" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="2022a471-10ba-4431-ba5d-622df898f3c6" name="org.iets3.core.expr.testExecution">
      <concept id="4473287864570292399" name="org.iets3.core.expr.testExecution.structure.TestExecutionConfig" flags="ng" index="3ZOQsN">
        <child id="4473287864570320758" name="executionMode" index="3ZOXzE" />
      </concept>
      <concept id="4473287864570320840" name="org.iets3.core.expr.testExecution.structure.InterpreterExecutionMode" flags="ng" index="3ZOXxk" />
    </language>
  </registry>
  <node concept="3ZOQsN" id="WHC6UIRfbv">
    <node concept="3ZOXxk" id="WHC6UIRfbx" role="3ZOXzE" />
  </node>
</model>

