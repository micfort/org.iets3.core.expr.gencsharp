<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:8424c1b1-c5cb-4768-a9e8-ef9ed432a8d9(org.micfort.require.generator.templates@generator)">
  <persistence version="9" />
  <languages>
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="4" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="rj8b" ref="r:b99735fa-1533-4a9e-bbdc-862af2a02d55(org.micfort.require.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="9mle" ref="r:6e9414c4-2fc5-46eb-beeb-018ba53d3106(org.micfort.require.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1219952072943" name="jetbrains.mps.lang.generator.structure.DropRootRule" flags="lg" index="aNPBN">
        <reference id="1219952338328" name="applicableConcept" index="aOQi4" />
      </concept>
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1219952894531" name="dropRootRule" index="aQYdv" />
        <child id="7473026166162327259" name="dropAttrubuteRule" index="CYSdJ" />
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="7473026166162297915" name="jetbrains.mps.lang.generator.structure.DropAttributeRule" flags="lg" index="CY16f">
        <reference id="7473026166162297918" name="applicableConcept" index="CY16a" />
      </concept>
      <concept id="1167168920554" name="jetbrains.mps.lang.generator.structure.BaseMappingRule_Condition" flags="in" index="30G5F_" />
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <property id="1167272244852" name="applyToConceptInheritors" index="36QftV" />
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
        <child id="1167169362365" name="conditionFunction" index="30HLyM" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="3562215692195599741" name="jetbrains.mps.lang.smodel.structure.SLinkImplicitSelect" flags="nn" index="13MTOL">
        <reference id="3562215692195600259" name="link" index="13MTZf" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4ejLsrQvEc7">
    <property role="TrG5h" value="main" />
    <node concept="aNPBN" id="4ejLsrQzrUA" role="aQYdv">
      <ref role="aOQi4" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
    </node>
    <node concept="aNPBN" id="4ejLsrQzrVl" role="aQYdv">
      <ref role="aOQi4" to="rj8b:4ejLsrQvEd1" resolve="AdditionTypeDefinition" />
    </node>
    <node concept="CY16f" id="4ejLsrQxZfA" role="CYSdJ">
      <ref role="CY16a" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
    </node>
    <node concept="CY16f" id="4ejLsrQxZgm" role="CYSdJ">
      <ref role="CY16a" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
    </node>
    <node concept="3aamgX" id="4ejLsrQxGnx" role="3acgRq">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="tpck:gw2VY9q" resolve="BaseConcept" />
      <node concept="gft3U" id="4ejLsrQxHkT" role="1lVwrX">
        <node concept="312cEu" id="4ejLsrQxHlH" role="gfFT$">
          <property role="TrG5h" value="replaced" />
          <node concept="3Tm1VV" id="4ejLsrQxHlI" role="1B3o_S" />
          <node concept="2b32R4" id="4ejLsrQxHn5" role="lGtFl">
            <node concept="3JmXsc" id="4ejLsrQxHn8" role="2P8S$">
              <node concept="3clFbS" id="4ejLsrQxHn9" role="2VODD2">
                <node concept="3clFbF" id="4ejLsrQxHnf" role="3cqZAp">
                  <node concept="2OqwBi" id="4ejLsrQxYFc" role="3clFbG">
                    <node concept="2OqwBi" id="4ejLsrQxY9K" role="2Oq$k0">
                      <node concept="2OqwBi" id="4ejLsrQxINc" role="2Oq$k0">
                        <node concept="30H73N" id="4ejLsrQxHne" role="2Oq$k0" />
                        <node concept="3CFZ6_" id="4ejLsrQxXQ$" role="2OqNvi">
                          <node concept="3CFYIy" id="4ejLsrQxXXG" role="3CFYIz">
                            <ref role="3CFYIx" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                          </node>
                        </node>
                      </node>
                      <node concept="2qgKlT" id="4ejLsrQxYno" role="2OqNvi">
                        <ref role="37wK5l" to="9mle:4ejLsrQyy_x" resolve="GetAllAdditions2" />
                      </node>
                    </node>
                    <node concept="13MTOL" id="4ejLsrQxYS2" role="2OqNvi">
                      <ref role="13MTZf" to="rj8b:4ejLsrQvEc9" resolve="content" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="4ejLsrQxGn_" role="30HLyM">
        <node concept="3clFbS" id="4ejLsrQxGnA" role="2VODD2">
          <node concept="3clFbF" id="4ejLsrQxGr_" role="3cqZAp">
            <node concept="2OqwBi" id="4ejLsrQxH2G" role="3clFbG">
              <node concept="2OqwBi" id="4ejLsrQxGC8" role="2Oq$k0">
                <node concept="30H73N" id="4ejLsrQxGr$" role="2Oq$k0" />
                <node concept="3CFZ6_" id="4ejLsrQxGML" role="2OqNvi">
                  <node concept="3CFYIy" id="4ejLsrQxGP3" role="3CFYIz">
                    <ref role="3CFYIx" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                  </node>
                </node>
              </node>
              <node concept="3x8VRR" id="4ejLsrQxHg0" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

