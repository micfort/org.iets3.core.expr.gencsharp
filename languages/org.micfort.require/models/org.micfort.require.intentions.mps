<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b7484254-ae23-44e5-afe7-b85509397048(org.micfort.require.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="rj8b" ref="r:b99735fa-1533-4a9e-bbdc-862af2a02d55(org.micfort.require.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC" />
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
      <concept id="1228341669568" name="jetbrains.mps.lang.smodel.structure.Node_DetachOperation" flags="nn" index="3YRAZt" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
    </language>
  </registry>
  <node concept="2S6QgY" id="4ejLsrQvEeI">
    <property role="TrG5h" value="ToggleRequire" />
    <ref role="2ZfgGC" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="2S6ZIM" id="4ejLsrQvEeJ" role="2ZfVej">
      <node concept="3clFbS" id="4ejLsrQvEeK" role="2VODD2">
        <node concept="3clFbF" id="4ejLsrQxhJU" role="3cqZAp">
          <node concept="3K4zz7" id="4ejLsrQxiXs" role="3clFbG">
            <node concept="Xl_RD" id="4ejLsrQxj09" role="3K4E3e">
              <property role="Xl_RC" value="Add Require" />
            </node>
            <node concept="Xl_RD" id="4ejLsrQxjkA" role="3K4GZi">
              <property role="Xl_RC" value="Remove Require" />
            </node>
            <node concept="2OqwBi" id="4ejLsrQxilF" role="3K4Cdx">
              <node concept="2OqwBi" id="4ejLsrQxhXi" role="2Oq$k0">
                <node concept="2Sf5sV" id="4ejLsrQxhJT" role="2Oq$k0" />
                <node concept="3CFZ6_" id="4ejLsrQxi6h" role="2OqNvi">
                  <node concept="3CFYIy" id="4ejLsrQxi8$" role="3CFYIz">
                    <ref role="3CFYIx" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                  </node>
                </node>
              </node>
              <node concept="3w_OXm" id="4ejLsrQxi$H" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4ejLsrQvEeL" role="2ZfgGD">
      <node concept="3clFbS" id="4ejLsrQvEeM" role="2VODD2">
        <node concept="3clFbJ" id="4ejLsrQvJjz" role="3cqZAp">
          <node concept="3clFbS" id="4ejLsrQvJj_" role="3clFbx">
            <node concept="3clFbF" id="4ejLsrQxtcC" role="3cqZAp">
              <node concept="2OqwBi" id="4ejLsrQxtSP" role="3clFbG">
                <node concept="2OqwBi" id="4ejLsrQxt_J" role="2Oq$k0">
                  <node concept="2Sf5sV" id="4ejLsrQxtcB" role="2Oq$k0" />
                  <node concept="3CFZ6_" id="4ejLsrQxtGR" role="2OqNvi">
                    <node concept="3CFYIy" id="4ejLsrQxtHk" role="3CFYIz">
                      <ref role="3CFYIx" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                    </node>
                  </node>
                </node>
                <node concept="zfrQC" id="4ejLsrQxu9a" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="4ejLsrQvOdS" role="9aQIa">
            <node concept="3clFbS" id="4ejLsrQvOdT" role="9aQI4">
              <node concept="3clFbF" id="4ejLsrQxuwW" role="3cqZAp">
                <node concept="2OqwBi" id="4ejLsrQxxdo" role="3clFbG">
                  <node concept="2OqwBi" id="4ejLsrQxwU_" role="2Oq$k0">
                    <node concept="2Sf5sV" id="4ejLsrQxuwV" role="2Oq$k0" />
                    <node concept="3CFZ6_" id="4ejLsrQxx47" role="2OqNvi">
                      <node concept="3CFYIy" id="4ejLsrQxx4B" role="3CFYIz">
                        <ref role="3CFYIx" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                      </node>
                    </node>
                  </node>
                  <node concept="3YRAZt" id="4ejLsrQxxeV" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4ejLsrQxq$x" role="3clFbw">
            <node concept="2OqwBi" id="4ejLsrQxmF1" role="2Oq$k0">
              <node concept="2Sf5sV" id="4ejLsrQxm7e" role="2Oq$k0" />
              <node concept="3CFZ6_" id="4ejLsrQxmX7" role="2OqNvi">
                <node concept="3CFYIy" id="4ejLsrQxnVv" role="3CFYIz">
                  <ref role="3CFYIx" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                </node>
              </node>
            </node>
            <node concept="3w_OXm" id="4ejLsrQxr$h" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="4ejLsrQw1dF">
    <property role="TrG5h" value="ToggleReplace" />
    <ref role="2ZfgGC" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="2S6ZIM" id="4ejLsrQw1dG" role="2ZfVej">
      <node concept="3clFbS" id="4ejLsrQw1dH" role="2VODD2">
        <node concept="3clFbF" id="4ejLsrQx9vj" role="3cqZAp">
          <node concept="3K4zz7" id="4ejLsrQxcnJ" role="3clFbG">
            <node concept="Xl_RD" id="4ejLsrQxcqn" role="3K4E3e">
              <property role="Xl_RC" value="Add Replace" />
            </node>
            <node concept="Xl_RD" id="4ejLsrQxczt" role="3K4GZi">
              <property role="Xl_RC" value="Remove Replace" />
            </node>
            <node concept="2OqwBi" id="4ejLsrQxbER" role="3K4Cdx">
              <node concept="2OqwBi" id="4ejLsrQx9GT" role="2Oq$k0">
                <node concept="2Sf5sV" id="4ejLsrQx9vi" role="2Oq$k0" />
                <node concept="3CFZ6_" id="4ejLsrQx9Wi" role="2OqNvi">
                  <node concept="3CFYIy" id="4ejLsrQxbmi" role="3CFYIz">
                    <ref role="3CFYIx" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                  </node>
                </node>
              </node>
              <node concept="3w_OXm" id="4ejLsrQxbVo" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="4ejLsrQw1dU" role="2ZfgGD">
      <node concept="3clFbS" id="4ejLsrQw1dV" role="2VODD2">
        <node concept="3clFbJ" id="4ejLsrQw1dW" role="3cqZAp">
          <node concept="3clFbS" id="4ejLsrQw1dX" role="3clFbx">
            <node concept="3clFbF" id="4ejLsrQxd9O" role="3cqZAp">
              <node concept="2OqwBi" id="4ejLsrQxesb" role="3clFbG">
                <node concept="2OqwBi" id="4ejLsrQxdDr" role="2Oq$k0">
                  <node concept="2Sf5sV" id="4ejLsrQxd9N" role="2Oq$k0" />
                  <node concept="3CFZ6_" id="4ejLsrQxdMg" role="2OqNvi">
                    <node concept="3CFYIy" id="4ejLsrQxdMK" role="3CFYIz">
                      <ref role="3CFYIx" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                    </node>
                  </node>
                </node>
                <node concept="zfrQC" id="4ejLsrQxeFk" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4ejLsrQw1e4" role="3clFbw">
            <node concept="2OqwBi" id="4ejLsrQw1e5" role="2Oq$k0">
              <node concept="2OqwBi" id="4ejLsrQw1e6" role="2Oq$k0">
                <node concept="2Sf5sV" id="4ejLsrQw1e7" role="2Oq$k0" />
                <node concept="3Tsc0h" id="4ejLsrQw1e8" role="2OqNvi">
                  <ref role="3TtcxE" to="tpck:4uZwTti3__2" resolve="smodelAttribute" />
                </node>
              </node>
              <node concept="v3k3i" id="4ejLsrQw1e9" role="2OqNvi">
                <node concept="chp4Y" id="4ejLsrQw1ea" role="v3oSu">
                  <ref role="cht4Q" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                </node>
              </node>
            </node>
            <node concept="1v1jN8" id="4ejLsrQw1eb" role="2OqNvi" />
          </node>
          <node concept="9aQIb" id="4ejLsrQw1ec" role="9aQIa">
            <node concept="3clFbS" id="4ejLsrQw1ed" role="9aQI4">
              <node concept="3clFbF" id="4ejLsrQxeIh" role="3cqZAp">
                <node concept="2OqwBi" id="4ejLsrQxf8t" role="3clFbG">
                  <node concept="2OqwBi" id="4ejLsrQxePl" role="2Oq$k0">
                    <node concept="2Sf5sV" id="4ejLsrQxeIg" role="2Oq$k0" />
                    <node concept="3CFZ6_" id="4ejLsrQxeWl" role="2OqNvi">
                      <node concept="3CFYIy" id="4ejLsrQxeWP" role="3CFYIz">
                        <ref role="3CFYIx" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
                      </node>
                    </node>
                  </node>
                  <node concept="3YRAZt" id="4ejLsrQxfni" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

