<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b99735fa-1533-4a9e-bbdc-862af2a02d55(org.micfort.require.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="6054523464626862044" name="jetbrains.mps.lang.structure.structure.AttributeInfo_IsMultiple" flags="ng" index="tn0Fv" />
      <concept id="6054523464627964745" name="jetbrains.mps.lang.structure.structure.AttributeInfo_AttributedConcept" flags="ng" index="trNpa">
        <reference id="6054523464627965081" name="concept" index="trN6q" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="2992811758677295509" name="jetbrains.mps.lang.structure.structure.AttributeInfo" flags="ng" index="M6xJ_">
        <property id="7588428831955550663" name="role" index="Hh88m" />
        <child id="7588428831947959310" name="attributed" index="EQaZv" />
        <child id="7588428831955550186" name="multiple" index="HhnKV" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4ejLsrQvEc8">
    <property role="EcuMT" value="4869453080447329032" />
    <property role="TrG5h" value="Addition" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="4ejLsrQvEc9" role="1TKVEi">
      <property role="IQ2ns" value="4869453080447329033" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="content" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" to="tpck:gw2VY9q" resolve="BaseConcept" />
    </node>
    <node concept="PrWs8" id="4ejLsrQvEcb" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="4ejLsrQvEd4" role="1TKVEi">
      <property role="IQ2ns" value="4869453080447329092" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4ejLsrQvEd1" resolve="AdditionTypeDefinition" />
    </node>
  </node>
  <node concept="1TIwiD" id="4ejLsrQvEd0">
    <property role="EcuMT" value="4869453080447329088" />
    <property role="TrG5h" value="ReplaceWithAdditions" />
    <ref role="1TJDcQ" to="tpck:2ULFgo8_XDk" resolve="NodeAttribute" />
    <node concept="1TJgyj" id="4ejLsrQvEd2" role="1TKVEi">
      <property role="IQ2ns" value="4869453080447329090" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4ejLsrQvEd1" resolve="AdditionTypeDefinition" />
    </node>
    <node concept="M6xJ_" id="4ejLsrQwb9$" role="lGtFl">
      <property role="Hh88m" value="replace" />
      <node concept="tn0Fv" id="4ejLsrQwb9C" role="HhnKV" />
      <node concept="trNpa" id="4ejLsrQwY4N" role="EQaZv">
        <ref role="trN6q" to="tpck:gw2VY9q" resolve="BaseConcept" />
      </node>
    </node>
    <node concept="1TJgyi" id="4ejLsrQyyH4" role="1TKVEl">
      <property role="IQ2nx" value="4869453080448084804" />
      <property role="TrG5h" value="AdditionsScope" />
      <ref role="AX2Wp" node="4ejLsrQyyn2" resolve="Scope" />
    </node>
  </node>
  <node concept="1TIwiD" id="4ejLsrQvEd1">
    <property role="EcuMT" value="4869453080447329089" />
    <property role="TrG5h" value="AdditionTypeDefinition" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="4ejLsrQw1bl" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4ejLsrQvEd7">
    <property role="EcuMT" value="4869453080447329095" />
    <property role="TrG5h" value="RequireAdditions" />
    <ref role="1TJDcQ" to="tpck:2ULFgo8_XDk" resolve="NodeAttribute" />
    <node concept="M6xJ_" id="4ejLsrQvEda" role="lGtFl">
      <property role="Hh88m" value="require" />
      <node concept="tn0Fv" id="4ejLsrQvEAH" role="HhnKV" />
      <node concept="trNpa" id="4ejLsrQwY4R" role="EQaZv">
        <ref role="trN6q" to="tpck:gw2VY9q" resolve="BaseConcept" />
      </node>
    </node>
    <node concept="1TJgyj" id="4ejLsrQvEAT" role="1TKVEi">
      <property role="IQ2ns" value="4869453080447330745" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="additions" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4ejLsrQvEAJ" resolve="AdditionReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4ejLsrQvEAJ">
    <property role="EcuMT" value="4869453080447330735" />
    <property role="TrG5h" value="AdditionReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4ejLsrQvEAK" role="1TKVEi">
      <property role="20lbJX" value="fLJekj4/1" />
      <property role="IQ2ns" value="4869453080447330736" />
      <property role="20kJfa" value="addition" />
      <ref role="20lvS9" node="4ejLsrQvEc8" resolve="Addition" />
    </node>
  </node>
  <node concept="25R3W" id="4ejLsrQyyn2">
    <property role="3F6X1D" value="4869453080448083394" />
    <property role="TrG5h" value="Scope" />
    <node concept="25R33" id="4ejLsrQyyn4" role="25R1y">
      <property role="3tVfz5" value="4869453080448083396" />
      <property role="TrG5h" value="Root" />
    </node>
    <node concept="25R33" id="4ejLsrQyyn7" role="25R1y">
      <property role="3tVfz5" value="4869453080448083399" />
      <property role="TrG5h" value="Model" />
    </node>
    <node concept="25R33" id="4ejLsrQyynb" role="25R1y">
      <property role="3tVfz5" value="4869453080448083403" />
      <property role="TrG5h" value="ModelAndIncludes" />
    </node>
  </node>
</model>

