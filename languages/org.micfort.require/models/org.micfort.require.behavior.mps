<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6e9414c4-2fc5-46eb-beeb-018ba53d3106(org.micfort.require.behavior)">
  <persistence version="9" />
  <languages>
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="19" />
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="2" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="rj8b" ref="r:b99735fa-1533-4a9e-bbdc-862af2a02d55(org.micfort.require.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="4693937538533521280" name="jetbrains.mps.lang.smodel.structure.OfConceptOperation" flags="ng" index="v3k3i">
        <child id="4693937538533538124" name="requestedConcept" index="v3oSu" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="1145383075378" name="jetbrains.mps.lang.smodel.structure.SNodeListType" flags="in" index="2I9FWS">
        <reference id="1145383142433" name="elementConcept" index="2I9WkF" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1171310072040" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingRootOperation" flags="nn" index="2Rxl7S" />
      <concept id="1171323947159" name="jetbrains.mps.lang.smodel.structure.Model_NodesOperation" flags="nn" index="2SmgA7">
        <child id="1758937410080001570" name="conceptArgument" index="1dBWTz" />
      </concept>
      <concept id="1145567426890" name="jetbrains.mps.lang.smodel.structure.SNodeListCreator" flags="nn" index="2T8Vx0">
        <child id="1145567471833" name="createdType" index="2T96Bj" />
      </concept>
      <concept id="1966870290088668512" name="jetbrains.mps.lang.smodel.structure.Enum_MemberLiteral" flags="ng" index="2ViDtV">
        <reference id="1966870290088668516" name="memberDeclaration" index="2ViDtZ" />
      </concept>
      <concept id="3562215692195599741" name="jetbrains.mps.lang.smodel.structure.SLinkImplicitSelect" flags="nn" index="13MTOL">
        <reference id="3562215692195600259" name="link" index="13MTZf" />
      </concept>
      <concept id="1182511038748" name="jetbrains.mps.lang.smodel.structure.Model_NodesIncludingImportedOperation" flags="nn" index="1j9C0f">
        <child id="6750920497477143623" name="conceptArgument" index="3MHPCF" />
      </concept>
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="2453008993612717253" name="jetbrains.mps.lang.smodel.structure.EnumSwitchCaseBody_Expression" flags="ng" index="3X5gDF">
        <child id="2453008993612717254" name="expression" index="3X5gDC" />
      </concept>
      <concept id="2453008993612559843" name="jetbrains.mps.lang.smodel.structure.EnumSwitchCase" flags="ng" index="3X5Udd">
        <child id="2453008993612717146" name="body" index="3X5gFO" />
        <child id="2453008993612559844" name="members" index="3X5Uda" />
      </concept>
      <concept id="2453008993612559839" name="jetbrains.mps.lang.smodel.structure.EnumSwitchExpression" flags="ng" index="3X5UdL">
        <child id="2453008993612714935" name="cases" index="3X5gkp" />
        <child id="2453008993612559840" name="enumExpression" index="3X5Ude" />
      </concept>
      <concept id="5779574625830813396" name="jetbrains.mps.lang.smodel.structure.EnumerationIdRefExpression" flags="ng" index="1XH99k">
        <reference id="5779574625830813397" name="enumDeclaration" index="1XH99l" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1176906603202" name="jetbrains.mps.baseLanguage.collections.structure.BinaryOperation" flags="nn" index="56pJg">
        <child id="1176906787974" name="rightExpression" index="576Qk" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
      <concept id="1180964022718" name="jetbrains.mps.baseLanguage.collections.structure.ConcatOperation" flags="nn" index="3QWeyG" />
      <concept id="1178894719932" name="jetbrains.mps.baseLanguage.collections.structure.DistinctOperation" flags="nn" index="1VAtEI" />
    </language>
  </registry>
  <node concept="13h7C7" id="4ejLsrQxIV4">
    <ref role="13h7C2" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
    <node concept="13i0hz" id="4ejLsrQyy_x" role="13h7CS">
      <property role="TrG5h" value="GetAllAdditions" />
      <node concept="3Tm1VV" id="4ejLsrQyy_y" role="1B3o_S" />
      <node concept="3clFbS" id="4ejLsrQyy_$" role="3clF47">
        <node concept="3cpWs8" id="4ejLsrQyZNF" role="3cqZAp">
          <node concept="3cpWsn" id="4ejLsrQyZNI" role="3cpWs9">
            <property role="TrG5h" value="scoped" />
            <node concept="A3Dl8" id="4ejLsrQyZNC" role="1tU5fm">
              <node concept="3Tqbb2" id="4ejLsrQz03d" role="A3Ik2">
                <ref role="ehGHo" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
              </node>
            </node>
            <node concept="3X5UdL" id="4ejLsrQz0pJ" role="33vP2m">
              <node concept="2OqwBi" id="4ejLsrQz0pK" role="3X5Ude">
                <node concept="13iPFW" id="4ejLsrQz0pL" role="2Oq$k0" />
                <node concept="3TrcHB" id="4ejLsrQz0pM" role="2OqNvi">
                  <ref role="3TsBF5" to="rj8b:4ejLsrQyyH4" resolve="AdditionsScope" />
                </node>
              </node>
              <node concept="3X5Udd" id="4ejLsrQz0pN" role="3X5gkp">
                <node concept="21nZrQ" id="4ejLsrQz0pO" role="3X5Uda">
                  <ref role="21nZrZ" to="rj8b:4ejLsrQyyn4" resolve="Root" />
                </node>
                <node concept="3X5gDF" id="4ejLsrQz0pP" role="3X5gFO">
                  <node concept="2OqwBi" id="4ejLsrQz0pQ" role="3X5gDC">
                    <node concept="2OqwBi" id="4ejLsrQz0pR" role="2Oq$k0">
                      <node concept="13iPFW" id="4ejLsrQz0pS" role="2Oq$k0" />
                      <node concept="2Rxl7S" id="4ejLsrQz0pT" role="2OqNvi" />
                    </node>
                    <node concept="2Rf3mk" id="4ejLsrQz0pU" role="2OqNvi">
                      <node concept="1xMEDy" id="4ejLsrQz0pV" role="1xVPHs">
                        <node concept="chp4Y" id="4ejLsrQz0pW" role="ri$Ld">
                          <ref role="cht4Q" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3X5Udd" id="4ejLsrQz0pX" role="3X5gkp">
                <node concept="21nZrQ" id="4ejLsrQz0pY" role="3X5Uda">
                  <ref role="21nZrZ" to="rj8b:4ejLsrQyyn7" resolve="Model" />
                </node>
                <node concept="3X5gDF" id="4ejLsrQz0pZ" role="3X5gFO">
                  <node concept="2OqwBi" id="4ejLsrQz0q0" role="3X5gDC">
                    <node concept="2OqwBi" id="4ejLsrQz0q1" role="2Oq$k0">
                      <node concept="13iPFW" id="4ejLsrQz0q2" role="2Oq$k0" />
                      <node concept="I4A8Y" id="4ejLsrQz0q3" role="2OqNvi" />
                    </node>
                    <node concept="2SmgA7" id="4ejLsrQz0q4" role="2OqNvi">
                      <node concept="chp4Y" id="4ejLsrQz0q5" role="1dBWTz">
                        <ref role="cht4Q" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3X5Udd" id="4ejLsrQz0q6" role="3X5gkp">
                <node concept="21nZrQ" id="4ejLsrQz0q7" role="3X5Uda">
                  <ref role="21nZrZ" to="rj8b:4ejLsrQyynb" resolve="ModelAndIncludes" />
                </node>
                <node concept="3X5gDF" id="4ejLsrQz0q8" role="3X5gFO">
                  <node concept="2OqwBi" id="4ejLsrQz0q9" role="3X5gDC">
                    <node concept="2OqwBi" id="4ejLsrQz0qa" role="2Oq$k0">
                      <node concept="13iPFW" id="4ejLsrQz0qb" role="2Oq$k0" />
                      <node concept="I4A8Y" id="4ejLsrQz0qc" role="2OqNvi" />
                    </node>
                    <node concept="1j9C0f" id="4ejLsrQz0qd" role="2OqNvi">
                      <node concept="chp4Y" id="4ejLsrQz0qe" role="3MHPCF">
                        <ref role="cht4Q" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4ejLsrQz109" role="3cqZAp">
          <node concept="2OqwBi" id="4ejLsrQz6mz" role="3clFbG">
            <node concept="2OqwBi" id="4ejLsrQz2Ch" role="2Oq$k0">
              <node concept="2OqwBi" id="4ejLsrQzQ2K" role="2Oq$k0">
                <node concept="2OqwBi" id="4ejLsrQz24A" role="2Oq$k0">
                  <node concept="2OqwBi" id="4ejLsrQz1u1" role="2Oq$k0">
                    <node concept="37vLTw" id="4ejLsrQz107" role="2Oq$k0">
                      <ref role="3cqZAo" node="4ejLsrQyZNI" resolve="scoped" />
                    </node>
                    <node concept="13MTOL" id="4ejLsrQz1I9" role="2OqNvi">
                      <ref role="13MTZf" to="rj8b:4ejLsrQvEAT" resolve="additions" />
                    </node>
                  </node>
                  <node concept="13MTOL" id="4ejLsrQz2jg" role="2OqNvi">
                    <ref role="13MTZf" to="rj8b:4ejLsrQvEAK" resolve="addition" />
                  </node>
                </node>
                <node concept="3goQfb" id="4ejLsrQzQb3" role="2OqNvi">
                  <node concept="1bVj0M" id="4ejLsrQzQb4" role="23t8la">
                    <node concept="3clFbS" id="4ejLsrQzQb5" role="1bW5cS">
                      <node concept="3clFbF" id="4ejLsrQzQb6" role="3cqZAp">
                        <node concept="2OqwBi" id="4ejLsrQzQb7" role="3clFbG">
                          <node concept="37vLTw" id="4ejLsrQzQb8" role="2Oq$k0">
                            <ref role="3cqZAo" node="4ejLsrQzQba" resolve="it" />
                          </node>
                          <node concept="2qgKlT" id="4ejLsrQzQb9" role="2OqNvi">
                            <ref role="37wK5l" node="4ejLsrQy4nq" resolve="GetAllAdditionsIncludingSelf" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="Rh6nW" id="4ejLsrQzQba" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="4ejLsrQzQbb" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3zZkjj" id="4ejLsrQz2Rp" role="2OqNvi">
                <node concept="1bVj0M" id="4ejLsrQz2Rr" role="23t8la">
                  <node concept="3clFbS" id="4ejLsrQz2Rs" role="1bW5cS">
                    <node concept="3clFbF" id="4ejLsrQz35M" role="3cqZAp">
                      <node concept="17R0WA" id="4ejLsrQz54P" role="3clFbG">
                        <node concept="2OqwBi" id="4ejLsrQz5$W" role="3uHU7w">
                          <node concept="13iPFW" id="4ejLsrQz5gP" role="2Oq$k0" />
                          <node concept="3TrEf2" id="4ejLsrQz60M" role="2OqNvi">
                            <ref role="3Tt5mk" to="rj8b:4ejLsrQvEd2" resolve="type" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="4ejLsrQz3oL" role="3uHU7B">
                          <node concept="37vLTw" id="4ejLsrQz35L" role="2Oq$k0">
                            <ref role="3cqZAo" node="4ejLsrQz2Rt" resolve="it" />
                          </node>
                          <node concept="3TrEf2" id="4ejLsrQz4$5" role="2OqNvi">
                            <ref role="3Tt5mk" to="rj8b:4ejLsrQvEd4" resolve="type" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="4ejLsrQz2Rt" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="4ejLsrQz2Ru" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1VAtEI" id="4ejLsrQz8hZ" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="A3Dl8" id="4ejLsrQyyGS" role="3clF45">
        <node concept="3Tqbb2" id="4ejLsrQyyGT" role="A3Ik2">
          <ref role="ehGHo" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4ejLsrQxIV5" role="13h7CW">
      <node concept="3clFbS" id="4ejLsrQxIV6" role="2VODD2">
        <node concept="3clFbF" id="4ejLsrQz8TH" role="3cqZAp">
          <node concept="37vLTI" id="4ejLsrQz9Kj" role="3clFbG">
            <node concept="2OqwBi" id="4ejLsrQzaqu" role="37vLTx">
              <node concept="1XH99k" id="4ejLsrQz9Yy" role="2Oq$k0">
                <ref role="1XH99l" to="rj8b:4ejLsrQyyn2" resolve="Scope" />
              </node>
              <node concept="2ViDtV" id="4ejLsrQzaGq" role="2OqNvi">
                <ref role="2ViDtZ" to="rj8b:4ejLsrQyyn4" resolve="Root" />
              </node>
            </node>
            <node concept="2OqwBi" id="4ejLsrQz92y" role="37vLTJ">
              <node concept="13iPFW" id="4ejLsrQz8TG" role="2Oq$k0" />
              <node concept="3TrcHB" id="4ejLsrQz9cz" role="2OqNvi">
                <ref role="3TsBF5" to="rj8b:4ejLsrQyyH4" resolve="AdditionsScope" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4ejLsrQy4eV">
    <ref role="13h7C2" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
    <node concept="13i0hz" id="4ejLsrQy4nq" role="13h7CS">
      <property role="TrG5h" value="GetAllAdditionsIncludingSelf" />
      <node concept="3Tm1VV" id="4ejLsrQy4nr" role="1B3o_S" />
      <node concept="3clFbS" id="4ejLsrQy4ns" role="3clF47">
        <node concept="3cpWs8" id="4ejLsrQybJY" role="3cqZAp">
          <node concept="3cpWsn" id="4ejLsrQybK1" role="3cpWs9">
            <property role="TrG5h" value="self" />
            <node concept="2I9FWS" id="4ejLsrQybJW" role="1tU5fm">
              <ref role="2I9WkF" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
            </node>
            <node concept="2ShNRf" id="4ejLsrQycih" role="33vP2m">
              <node concept="2T8Vx0" id="4ejLsrQycsP" role="2ShVmc">
                <node concept="2I9FWS" id="4ejLsrQycsR" role="2T96Bj">
                  <ref role="2I9WkF" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4ejLsrQyd3e" role="3cqZAp">
          <node concept="2OqwBi" id="4ejLsrQyeK3" role="3clFbG">
            <node concept="37vLTw" id="4ejLsrQyd3c" role="2Oq$k0">
              <ref role="3cqZAo" node="4ejLsrQybK1" resolve="self" />
            </node>
            <node concept="TSZUe" id="4ejLsrQyhZb" role="2OqNvi">
              <node concept="13iPFW" id="4ejLsrQyjae" role="25WWJ7" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4ejLsrQy5p3" role="3cqZAp">
          <node concept="2OqwBi" id="4ejLsrQyjI8" role="3clFbG">
            <node concept="2OqwBi" id="4ejLsrQy6bS" role="2Oq$k0">
              <node concept="2OqwBi" id="4ejLsrQy5p5" role="2Oq$k0">
                <node concept="2OqwBi" id="4ejLsrQy5p6" role="2Oq$k0">
                  <node concept="2OqwBi" id="4ejLsrQy5p7" role="2Oq$k0">
                    <node concept="2OqwBi" id="4ejLsrQy5p8" role="2Oq$k0">
                      <node concept="2OqwBi" id="4ejLsrQy5p9" role="2Oq$k0">
                        <node concept="13iPFW" id="4ejLsrQy5pb" role="2Oq$k0" />
                        <node concept="2Rf3mk" id="4ejLsrQy5pd" role="2OqNvi">
                          <node concept="1xMEDy" id="4ejLsrQy5pe" role="1xVPHs">
                            <node concept="chp4Y" id="4ejLsrQy5pf" role="ri$Ld">
                              <ref role="cht4Q" to="tpck:gw2VY9q" resolve="BaseConcept" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3$u5V9" id="4ejLsrQy5pg" role="2OqNvi">
                        <node concept="1bVj0M" id="4ejLsrQy5ph" role="23t8la">
                          <node concept="3clFbS" id="4ejLsrQy5pi" role="1bW5cS">
                            <node concept="3clFbF" id="4ejLsrQy5pj" role="3cqZAp">
                              <node concept="2OqwBi" id="4ejLsrQy5pk" role="3clFbG">
                                <node concept="37vLTw" id="4ejLsrQy5pl" role="2Oq$k0">
                                  <ref role="3cqZAo" node="4ejLsrQy5po" resolve="it" />
                                </node>
                                <node concept="3CFZ6_" id="4ejLsrQy5pm" role="2OqNvi">
                                  <node concept="3CFYIy" id="4ejLsrQy5pn" role="3CFYIz">
                                    <ref role="3CFYIx" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="4ejLsrQy5po" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="4ejLsrQy5pp" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="v3k3i" id="4ejLsrQy5pq" role="2OqNvi">
                      <node concept="chp4Y" id="4ejLsrQy5pr" role="v3oSu">
                        <ref role="cht4Q" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
                      </node>
                    </node>
                  </node>
                  <node concept="13MTOL" id="4ejLsrQy5ps" role="2OqNvi">
                    <ref role="13MTZf" to="rj8b:4ejLsrQvEAT" resolve="additions" />
                  </node>
                </node>
                <node concept="13MTOL" id="4ejLsrQy5pt" role="2OqNvi">
                  <ref role="13MTZf" to="rj8b:4ejLsrQvEAK" resolve="addition" />
                </node>
              </node>
              <node concept="3goQfb" id="4ejLsrQy6qz" role="2OqNvi">
                <node concept="1bVj0M" id="4ejLsrQy6q_" role="23t8la">
                  <node concept="3clFbS" id="4ejLsrQy6qA" role="1bW5cS">
                    <node concept="3clFbF" id="4ejLsrQy6xl" role="3cqZAp">
                      <node concept="2OqwBi" id="4ejLsrQy6Ku" role="3clFbG">
                        <node concept="37vLTw" id="4ejLsrQy6xk" role="2Oq$k0">
                          <ref role="3cqZAo" node="4ejLsrQy6qB" resolve="it" />
                        </node>
                        <node concept="2qgKlT" id="4ejLsrQy775" role="2OqNvi">
                          <ref role="37wK5l" node="4ejLsrQy4nq" resolve="GetAllAdditionsIncludingSelf" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="4ejLsrQy6qB" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="4ejLsrQy6qC" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3QWeyG" id="4ejLsrQym3l" role="2OqNvi">
              <node concept="37vLTw" id="4ejLsrQymi$" role="576Qk">
                <ref role="3cqZAo" node="4ejLsrQybK1" resolve="self" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="A3Dl8" id="4ejLsrQy4nT" role="3clF45">
        <node concept="3Tqbb2" id="4ejLsrQy4nU" role="A3Ik2">
          <ref role="ehGHo" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4ejLsrQy4eW" role="13h7CW">
      <node concept="3clFbS" id="4ejLsrQy4eX" role="2VODD2" />
    </node>
  </node>
</model>

