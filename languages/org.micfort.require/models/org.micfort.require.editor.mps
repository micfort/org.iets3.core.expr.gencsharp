<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:818427be-beae-4d3d-81f2-259d3bd89525(org.micfort.require.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="rj8b" ref="r:b99735fa-1533-4a9e-bbdc-862af2a02d55(org.micfort.require.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="9mle" ref="r:6e9414c4-2fc5-46eb-beeb-018ba53d3106(org.micfort.require.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1149850725784" name="jetbrains.mps.lang.editor.structure.CellModel_AttributedNodeCell" flags="ng" index="2SsqMj" />
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styles" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2" />
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414999511" name="jetbrains.mps.lang.editor.structure.UnderlinedStyleClassItem" flags="ln" index="VQ3r3">
        <property id="1214316229833" name="underlined" index="2USNnj" />
      </concept>
      <concept id="3383245079137382180" name="jetbrains.mps.lang.editor.structure.StyleClass" flags="ig" index="14StLt" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="9122903797312246523" name="jetbrains.mps.lang.editor.structure.StyleReference" flags="ng" index="1wgc9g">
        <reference id="9122903797312247166" name="style" index="1wgcnl" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1950447826681509042" name="jetbrains.mps.lang.editor.structure.ApplyStyleClass" flags="lg" index="3Xmtl4">
        <child id="1950447826683828796" name="target" index="3XvnJa" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1240687580870" name="jetbrains.mps.baseLanguage.collections.structure.JoinOperation" flags="nn" index="3uJxvA">
        <child id="1240687658305" name="delimiter" index="3uJOhx" />
      </concept>
      <concept id="1202128969694" name="jetbrains.mps.baseLanguage.collections.structure.SelectOperation" flags="nn" index="3$u5V9" />
    </language>
  </registry>
  <node concept="24kQdi" id="4ejLsrQvEck">
    <ref role="1XX52x" to="rj8b:4ejLsrQvEc8" resolve="Addition" />
    <node concept="3EZMnI" id="4ejLsrQvEcp" role="2wV5jI">
      <node concept="l2Vlx" id="4ejLsrQvEcq" role="2iSdaV" />
      <node concept="3F0ifn" id="4ejLsrQvEcm" role="3EZMnx">
        <property role="3F0ifm" value="Addition" />
      </node>
      <node concept="3F0A7n" id="4ejLsrQvEcy" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="4ejLsrQw1aN" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="1iCGBv" id="4ejLsrQw1b5" role="3EZMnx">
        <ref role="1NtTu8" to="rj8b:4ejLsrQvEd4" resolve="type" />
        <node concept="1sVBvm" id="4ejLsrQw1b7" role="1sWHZn">
          <node concept="3F0A7n" id="4ejLsrQw1bn" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4ejLsrQvEcR" role="3EZMnx">
        <node concept="pVoyu" id="4ejLsrQw1aq" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F1sOY" id="4ejLsrQvEcE" role="3EZMnx">
        <ref role="1NtTu8" to="rj8b:4ejLsrQvEc9" resolve="content" />
        <node concept="pVoyu" id="4ejLsrQw1av" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
    <node concept="3EZMnI" id="4ejLsrQzXci" role="6VMZX">
      <node concept="l2Vlx" id="4ejLsrQzXcj" role="2iSdaV" />
      <node concept="3F0ifn" id="4ejLsrQzXrX" role="3EZMnx">
        <property role="3F0ifm" value="Additions included: " />
      </node>
      <node concept="1HlG4h" id="4ejLsrQzC10" role="3EZMnx">
        <node concept="1HfYo3" id="4ejLsrQzC12" role="1HlULh">
          <node concept="3TQlhw" id="4ejLsrQzC14" role="1Hhtcw">
            <node concept="3clFbS" id="4ejLsrQzC16" role="2VODD2">
              <node concept="3clFbF" id="4ejLsrQzC5N" role="3cqZAp">
                <node concept="2OqwBi" id="4ejLsrQzDMS" role="3clFbG">
                  <node concept="2OqwBi" id="4ejLsrQzCIW" role="2Oq$k0">
                    <node concept="2OqwBi" id="4ejLsrQzCis" role="2Oq$k0">
                      <node concept="pncrf" id="4ejLsrQzC5M" role="2Oq$k0" />
                      <node concept="2qgKlT" id="4ejLsrQzCud" role="2OqNvi">
                        <ref role="37wK5l" to="9mle:4ejLsrQy4nq" resolve="GetAllAdditionsIncludingSelf" />
                      </node>
                    </node>
                    <node concept="3$u5V9" id="4ejLsrQzD0K" role="2OqNvi">
                      <node concept="1bVj0M" id="4ejLsrQzD0M" role="23t8la">
                        <node concept="3clFbS" id="4ejLsrQzD0N" role="1bW5cS">
                          <node concept="3clFbF" id="4ejLsrQzDa4" role="3cqZAp">
                            <node concept="2OqwBi" id="4ejLsrQzDkH" role="3clFbG">
                              <node concept="37vLTw" id="4ejLsrQzDa3" role="2Oq$k0">
                                <ref role="3cqZAo" node="4ejLsrQzD0O" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="4ejLsrQzD$K" role="2OqNvi">
                                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4ejLsrQzD0O" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4ejLsrQzD0P" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3uJxvA" id="4ejLsrQzEda" role="2OqNvi">
                    <node concept="Xl_RD" id="4ejLsrQzENg" role="3uJOhx">
                      <property role="Xl_RC" value=", " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4ejLsrQvEdn">
    <ref role="1XX52x" to="rj8b:4ejLsrQvEd7" resolve="RequireAdditions" />
    <node concept="3EZMnI" id="4ejLsrQvEdp" role="2wV5jI">
      <node concept="3F0ifn" id="4ejLsrQvEdw" role="3EZMnx">
        <property role="3F0ifm" value="&lt;require" />
        <ref role="1k5W1q" node="4ejLsrQw1c3" resolve="AttributeText" />
      </node>
      <node concept="l2Vlx" id="4ejLsrQvEds" role="2iSdaV" />
      <node concept="2SsqMj" id="4ejLsrQvEdA" role="3EZMnx" />
      <node concept="3F0ifn" id="4ejLsrQvEdI" role="3EZMnx">
        <property role="3F0ifm" value="&gt;" />
        <ref role="1k5W1q" node="4ejLsrQw1c3" resolve="AttributeText" />
      </node>
    </node>
    <node concept="3EZMnI" id="4ejLsrQvEe9" role="6VMZX">
      <node concept="l2Vlx" id="4ejLsrQvEea" role="2iSdaV" />
      <node concept="3F0ifn" id="4ejLsrQvEew" role="3EZMnx">
        <property role="3F0ifm" value="Additions:" />
      </node>
      <node concept="3F2HdR" id="4ejLsrQvEB1" role="3EZMnx">
        <ref role="1NtTu8" to="rj8b:4ejLsrQvEAT" resolve="additions" />
        <node concept="l2Vlx" id="4ejLsrQvEB3" role="2czzBx" />
        <node concept="pVoyu" id="4ejLsrQvEB7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="4ejLsrQvEBc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="4ejLsrQvEBk" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4ejLsrQvEAL">
    <ref role="1XX52x" to="rj8b:4ejLsrQvEAJ" resolve="AdditionReference" />
    <node concept="1iCGBv" id="4ejLsrQvEAM" role="2wV5jI">
      <ref role="1NtTu8" to="rj8b:4ejLsrQvEAK" resolve="addition" />
      <node concept="1sVBvm" id="4ejLsrQvEAN" role="1sWHZn">
        <node concept="3F0A7n" id="4ejLsrQvEAO" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4ejLsrQw1bx">
    <ref role="1XX52x" to="rj8b:4ejLsrQvEd1" resolve="AdditionTypeDefinition" />
    <node concept="3EZMnI" id="4ejLsrQw1bG" role="2wV5jI">
      <node concept="l2Vlx" id="4ejLsrQw1bH" role="2iSdaV" />
      <node concept="3F0ifn" id="4ejLsrQw1bD" role="3EZMnx">
        <property role="3F0ifm" value="Addition Type" />
      </node>
      <node concept="3F0A7n" id="4ejLsrQw1bP" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4ejLsrQw1c0">
    <ref role="1XX52x" to="rj8b:4ejLsrQvEd0" resolve="ReplaceWithAdditions" />
    <node concept="3EZMnI" id="4ejLsrQw1ck" role="2wV5jI">
      <node concept="3F0ifn" id="4ejLsrQw1cr" role="3EZMnx">
        <property role="3F0ifm" value="&lt;replace with" />
        <ref role="1k5W1q" node="4ejLsrQw1c3" resolve="AttributeText" />
      </node>
      <node concept="1iCGBv" id="4ejLsrQw1cx" role="3EZMnx">
        <ref role="1NtTu8" to="rj8b:4ejLsrQvEd2" resolve="type" />
        <node concept="1sVBvm" id="4ejLsrQw1cz" role="1sWHZn">
          <node concept="3F0A7n" id="4ejLsrQw1cJ" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
            <ref role="1k5W1q" node="4ejLsrQxyRr" resolve="AttributeTextReference" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="4ejLsrQw1cS" role="3EZMnx">
        <property role="3F0ifm" value=":" />
        <ref role="1k5W1q" node="4ejLsrQw1c3" resolve="AttributeText" />
      </node>
      <node concept="2SsqMj" id="4ejLsrQw1d6" role="3EZMnx" />
      <node concept="3F0ifn" id="4ejLsrQw1dm" role="3EZMnx">
        <property role="3F0ifm" value="&gt;" />
        <ref role="1k5W1q" node="4ejLsrQw1c3" resolve="AttributeText" />
      </node>
      <node concept="l2Vlx" id="4ejLsrQw1cn" role="2iSdaV" />
    </node>
    <node concept="3EZMnI" id="4ejLsrQzaIp" role="6VMZX">
      <node concept="l2Vlx" id="4ejLsrQzaIq" role="2iSdaV" />
      <node concept="3F0ifn" id="4ejLsrQzaIt" role="3EZMnx">
        <property role="3F0ifm" value="Scope:" />
      </node>
      <node concept="3F0A7n" id="4ejLsrQzaIM" role="3EZMnx">
        <ref role="1NtTu8" to="rj8b:4ejLsrQyyH4" resolve="AdditionsScope" />
        <node concept="ljvvj" id="4ejLsrQzXYZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="4ejLsrQzXKO" role="3EZMnx">
        <property role="3F0ifm" value="Replaced by additions:" />
      </node>
      <node concept="1HlG4h" id="4ejLsrQzth5" role="3EZMnx">
        <node concept="1HfYo3" id="4ejLsrQzth7" role="1HlULh">
          <node concept="3TQlhw" id="4ejLsrQzth9" role="1Hhtcw">
            <node concept="3clFbS" id="4ejLsrQzthb" role="2VODD2">
              <node concept="3clFbF" id="4ejLsrQztm2" role="3cqZAp">
                <node concept="2OqwBi" id="4ejLsrQzw3V" role="3clFbG">
                  <node concept="2OqwBi" id="4ejLsrQzuUQ" role="2Oq$k0">
                    <node concept="2OqwBi" id="4ejLsrQztzz" role="2Oq$k0">
                      <node concept="pncrf" id="4ejLsrQztm1" role="2Oq$k0" />
                      <node concept="2qgKlT" id="4ejLsrQztU4" role="2OqNvi">
                        <ref role="37wK5l" to="9mle:4ejLsrQyy_x" resolve="GetAllAdditions" />
                      </node>
                    </node>
                    <node concept="3$u5V9" id="4ejLsrQzvqU" role="2OqNvi">
                      <node concept="1bVj0M" id="4ejLsrQzvqW" role="23t8la">
                        <node concept="3clFbS" id="4ejLsrQzvqX" role="1bW5cS">
                          <node concept="3clFbF" id="4ejLsrQzvr7" role="3cqZAp">
                            <node concept="2OqwBi" id="4ejLsrQzv_K" role="3clFbG">
                              <node concept="37vLTw" id="4ejLsrQzvr6" role="2Oq$k0">
                                <ref role="3cqZAo" node="4ejLsrQzvqY" resolve="it" />
                              </node>
                              <node concept="3TrcHB" id="4ejLsrQzvPN" role="2OqNvi">
                                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Rh6nW" id="4ejLsrQzvqY" role="1bW2Oz">
                          <property role="TrG5h" value="it" />
                          <node concept="2jxLKc" id="4ejLsrQzvqZ" role="1tU5fm" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3uJxvA" id="4ejLsrQzwwg" role="2OqNvi">
                    <node concept="Xl_RD" id="4ejLsrQzxu0" role="3uJOhx">
                      <property role="Xl_RC" value=", " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="V5hpn" id="4ejLsrQw1c2">
    <property role="TrG5h" value="RequireStyles" />
    <node concept="14StLt" id="4ejLsrQw1c3" role="V601i">
      <property role="TrG5h" value="AttributeText" />
      <node concept="Vb9p2" id="4ejLsrQw1cg" role="3F10Kt" />
      <node concept="VechU" id="4ejLsrQw1ch" role="3F10Kt">
        <property role="Vb096" value="fLJRk5A/lightGray" />
      </node>
    </node>
    <node concept="14StLt" id="4ejLsrQxyRr" role="V601i">
      <property role="TrG5h" value="AttributeTextReference" />
      <node concept="3Xmtl4" id="4ejLsrQxyRx" role="3F10Kt">
        <node concept="1wgc9g" id="4ejLsrQxyRy" role="3XvnJa">
          <ref role="1wgcnl" node="4ejLsrQw1c3" resolve="AttributeText" />
        </node>
      </node>
      <node concept="VQ3r3" id="4ejLsrQxyRO" role="3F10Kt">
        <property role="2USNnj" value="gtbM8PH/underlined" />
      </node>
    </node>
  </node>
</model>

