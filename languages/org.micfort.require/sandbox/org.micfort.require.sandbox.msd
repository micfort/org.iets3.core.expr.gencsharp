<?xml version="1.0" encoding="UTF-8"?>
<solution name="org.micfort.require.sandbox" uuid="1b8d1a39-7172-4fd7-afa9-c0c762c66ae1" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions>
    <language slang="l:990507d3-3527-4c54-bfe9-0ca3c9c6247a:com.dslfoundry.plaintextgen" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="2" />
    <language slang="l:587dd766-0931-4308-b728-f045c58504e1:org.micfort.require" version="0" />
  </languageVersions>
  <dependencyVersions>
    <module reference="1b8d1a39-7172-4fd7-afa9-c0c762c66ae1(org.micfort.require.sandbox)" version="0" />
  </dependencyVersions>
</solution>

