<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1165fa58-e0e5-46d9-bd61-ed66b3262802(org.micfort.require.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="587dd766-0931-4308-b728-f045c58504e1" name="org.micfort.require" version="0" />
    <use id="990507d3-3527-4c54-bfe9-0ca3c9c6247a" name="com.dslfoundry.plaintextgen" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="587dd766-0931-4308-b728-f045c58504e1" name="org.micfort.require">
      <concept id="4869453080447329095" name="org.micfort.require.structure.RequireAdditions" flags="ng" index="2PVmq1">
        <child id="4869453080447330745" name="additions" index="2PVmLZ" />
      </concept>
      <concept id="4869453080447329088" name="org.micfort.require.structure.ReplaceWithAdditions" flags="ng" index="2PVmq6">
        <property id="4869453080448084804" name="AdditionsScope" index="2P6uU2" />
        <reference id="4869453080447329090" name="type" index="2PVmq4" />
      </concept>
      <concept id="4869453080447329089" name="org.micfort.require.structure.AdditionTypeDefinition" flags="ng" index="2PVmq7" />
      <concept id="4869453080447329032" name="org.micfort.require.structure.Addition" flags="ng" index="2PVmre">
        <reference id="4869453080447329092" name="type" index="2PVmq2" />
        <child id="4869453080447329033" name="content" index="2PVmrf" />
      </concept>
      <concept id="4869453080447330735" name="org.micfort.require.structure.AdditionReference" flags="ng" index="2PVmLD">
        <reference id="4869453080447330736" name="addition" index="2PVmLQ" />
      </concept>
    </language>
    <language id="990507d3-3527-4c54-bfe9-0ca3c9c6247a" name="com.dslfoundry.plaintextgen">
      <concept id="5082088080656902716" name="com.dslfoundry.plaintextgen.structure.NewlineMarker" flags="ng" index="2EixSi" />
      <concept id="1145195647825954804" name="com.dslfoundry.plaintextgen.structure.word" flags="ng" index="356sEF" />
      <concept id="1145195647825954799" name="com.dslfoundry.plaintextgen.structure.Line" flags="ng" index="356sEK">
        <child id="5082088080656976323" name="newlineMarker" index="2EinRH" />
        <child id="1145195647825954802" name="words" index="356sEH" />
      </concept>
      <concept id="1145195647825954793" name="com.dslfoundry.plaintextgen.structure.SpaceIndentedText" flags="ng" index="356sEQ">
        <property id="5198309202558919052" name="indent" index="333NGx" />
      </concept>
      <concept id="1145195647825954788" name="com.dslfoundry.plaintextgen.structure.TextgenText" flags="ng" index="356sEV">
        <property id="5407518469085446424" name="ext" index="3Le9LX" />
        <child id="1145195647826100986" name="content" index="356KY_" />
      </concept>
      <concept id="1145195647826084325" name="com.dslfoundry.plaintextgen.structure.VerticalLines" flags="ng" index="356WMU" />
      <concept id="7214912913997260680" name="com.dslfoundry.plaintextgen.structure.IVerticalGroup" flags="ng" index="383Yap">
        <child id="7214912913997260696" name="lines" index="383Ya9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="356sEV" id="4ejLsrQwJYu">
    <property role="TrG5h" value="ShieldedTest" />
    <property role="3Le9LX" value=".cs" />
    <node concept="356WMU" id="4ejLsrQwJYw" role="356KY_">
      <node concept="356sEK" id="4ejLsrQwJYx" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwJYy" role="356sEH">
          <property role="TrG5h" value="using System.Diagnostics.Tracing;" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwJY$" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwJY_" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwJYA" role="356sEH">
          <property role="TrG5h" value="using System.Transactions;" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwJYC" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwK4f" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwK5W" role="356sEH">
          <property role="TrG5h" value="extra usings" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwK4h" role="2EinRH" />
        <node concept="2PVmq6" id="4ejLsrQwTOR" role="lGtFl">
          <property role="2P6uU2" value="4ejLsrQyyn4/Root" />
          <ref role="2PVmq4" node="4ejLsrQwK2t" resolve="C# using" />
        </node>
      </node>
      <node concept="356sEK" id="4ejLsrQwJYH" role="383Ya9">
        <node concept="2EixSi" id="4ejLsrQwJYK" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwJYL" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwJYM" role="356sEH">
          <property role="TrG5h" value="namespace Sandbox;" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwJYO" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwJYP" role="383Ya9">
        <node concept="2EixSi" id="4ejLsrQwJYS" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwJYT" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwJYU" role="356sEH">
          <property role="TrG5h" value="public static class ShieldedTest" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwJYW" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQwJYX" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwJYY" role="356sEH">
          <property role="TrG5h" value="{" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwJZ0" role="2EinRH" />
      </node>
      <node concept="356sEQ" id="4ejLsrQwJZ5" role="383Ya9">
        <property role="333NGx" value="    " />
        <node concept="356sEK" id="4ejLsrQwJZ1" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwJZ2" role="356sEH">
            <property role="TrG5h" value="public static readonly Shielded&lt;int&gt; A = new Shielded&lt;int&gt;(10);" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwJZ4" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwJZ6" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwJZ7" role="356sEH">
            <property role="TrG5h" value="public static readonly AutoResetEvent resetEvent = new(false);" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwJZ9" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwJZa" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwJZb" role="356sEH">
            <property role="TrG5h" value="public static readonly AutoResetEvent resetEventBack = new(false);" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwJZd" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwJZe" role="383Ya9">
          <node concept="2EixSi" id="4ejLsrQwJZh" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwJZi" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwJZj" role="356sEH">
            <property role="TrG5h" value="public static void Test()" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwJZl" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwJZm" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwJZn" role="356sEH">
            <property role="TrG5h" value="{" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwJZp" role="2EinRH" />
        </node>
        <node concept="356sEQ" id="4ejLsrQwJZu" role="383Ya9">
          <property role="333NGx" value="    " />
          <node concept="356sEK" id="4ejLsrQwJZq" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwJZr" role="356sEH">
              <property role="TrG5h" value="var partner = Task.Factory.StartNew(TestPartner);" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwJZt" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwJZv" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwJZw" role="356sEH">
              <property role="TrG5h" value="Shield.InTransaction(() =&gt;" />
              <node concept="2PVmq1" id="4ejLsrQxGnt" role="lGtFl">
                <node concept="2PVmLD" id="4ejLsrQxGnv" role="2PVmLZ">
                  <ref role="2PVmLQ" node="4ejLsrQwK2u" resolve="Shielded" />
                </node>
              </node>
            </node>
            <node concept="2EixSi" id="4ejLsrQwJZy" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwJZz" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwJZ$" role="356sEH">
              <property role="TrG5h" value="{" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwJZA" role="2EinRH" />
          </node>
          <node concept="356sEQ" id="4ejLsrQwJZF" role="383Ya9">
            <property role="333NGx" value="    " />
            <node concept="356sEK" id="4ejLsrQwJZB" role="383Ya9">
              <node concept="356sEF" id="4ejLsrQwJZC" role="356sEH">
                <property role="TrG5h" value="Console.Out.WriteLine($&quot;{A.Value}&quot;);" />
              </node>
              <node concept="2EixSi" id="4ejLsrQwJZE" role="2EinRH" />
            </node>
            <node concept="356sEK" id="4ejLsrQwJZG" role="383Ya9">
              <node concept="356sEF" id="4ejLsrQwJZH" role="356sEH">
                <property role="TrG5h" value="resetEvent.Set();" />
              </node>
              <node concept="2EixSi" id="4ejLsrQwJZJ" role="2EinRH" />
            </node>
            <node concept="356sEK" id="4ejLsrQwJZK" role="383Ya9">
              <node concept="356sEF" id="4ejLsrQwJZL" role="356sEH">
                <property role="TrG5h" value="resetEventBack.WaitOne();" />
              </node>
              <node concept="2EixSi" id="4ejLsrQwJZN" role="2EinRH" />
            </node>
            <node concept="356sEK" id="4ejLsrQwJZO" role="383Ya9">
              <node concept="356sEF" id="4ejLsrQwJZP" role="356sEH">
                <property role="TrG5h" value="Console.Out.WriteLine($&quot;{A.Value}&quot;);" />
              </node>
              <node concept="2EixSi" id="4ejLsrQwJZR" role="2EinRH" />
            </node>
          </node>
          <node concept="356sEK" id="4ejLsrQwJZS" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwJZT" role="356sEH">
              <property role="TrG5h" value="});" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwJZV" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwJZW" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwJZX" role="356sEH">
              <property role="TrG5h" value="Console.Out.WriteLine($&quot;{A.Value}&quot;);" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwJZZ" role="2EinRH" />
          </node>
        </node>
        <node concept="356sEK" id="4ejLsrQwK00" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwK01" role="356sEH">
            <property role="TrG5h" value="}" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwK03" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwK04" role="383Ya9">
          <node concept="2EixSi" id="4ejLsrQwK07" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwK08" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwK09" role="356sEH">
            <property role="TrG5h" value="public static void TestPartner()" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwK0b" role="2EinRH" />
        </node>
        <node concept="356sEK" id="4ejLsrQwK0c" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwK0d" role="356sEH">
            <property role="TrG5h" value="{" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwK0f" role="2EinRH" />
        </node>
        <node concept="356sEQ" id="4ejLsrQwK0k" role="383Ya9">
          <property role="333NGx" value="    " />
          <node concept="356sEK" id="4ejLsrQwK0g" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwK0h" role="356sEH">
              <property role="TrG5h" value="resetEvent.WaitOne();" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwK0j" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwK0l" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwK0m" role="356sEH">
              <property role="TrG5h" value="Shield.InTransaction(() =&gt;" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwK0o" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwK0p" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwK0q" role="356sEH">
              <property role="TrG5h" value="{" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwK0s" role="2EinRH" />
          </node>
          <node concept="356sEQ" id="4ejLsrQwK0x" role="383Ya9">
            <property role="333NGx" value="    " />
            <node concept="356sEK" id="4ejLsrQwK0t" role="383Ya9">
              <node concept="356sEF" id="4ejLsrQwK0u" role="356sEH">
                <property role="TrG5h" value="A.Value = A.Value+1;" />
              </node>
              <node concept="2EixSi" id="4ejLsrQwK0w" role="2EinRH" />
            </node>
          </node>
          <node concept="356sEK" id="4ejLsrQwK0y" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwK0z" role="356sEH">
              <property role="TrG5h" value="});" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwK0_" role="2EinRH" />
          </node>
          <node concept="356sEK" id="4ejLsrQwK0A" role="383Ya9">
            <node concept="356sEF" id="4ejLsrQwK0B" role="356sEH">
              <property role="TrG5h" value="resetEventBack.Set();" />
            </node>
            <node concept="2EixSi" id="4ejLsrQwK0D" role="2EinRH" />
          </node>
        </node>
        <node concept="356sEK" id="4ejLsrQwK0E" role="383Ya9">
          <node concept="356sEF" id="4ejLsrQwK0F" role="356sEH">
            <property role="TrG5h" value="}" />
          </node>
          <node concept="2EixSi" id="4ejLsrQwK0H" role="2EinRH" />
        </node>
      </node>
      <node concept="356sEK" id="4ejLsrQwK0I" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQwK0J" role="356sEH">
          <property role="TrG5h" value="}" />
        </node>
        <node concept="2EixSi" id="4ejLsrQwK0L" role="2EinRH" />
      </node>
    </node>
  </node>
  <node concept="2PVmq7" id="4ejLsrQwK2t">
    <property role="TrG5h" value="C# using" />
  </node>
  <node concept="2PVmre" id="4ejLsrQwK2u">
    <property role="TrG5h" value="Shielded" />
    <ref role="2PVmq2" node="4ejLsrQwK2t" resolve="Using" />
    <node concept="356sEK" id="4ejLsrQwK2w" role="2PVmrf">
      <node concept="356sEF" id="4ejLsrQwK2x" role="356sEH">
        <property role="TrG5h" value="using Shielded;" />
      </node>
      <node concept="2EixSi" id="4ejLsrQwK2y" role="2EinRH" />
    </node>
  </node>
  <node concept="2PVmq7" id="4ejLsrQyyn1">
    <property role="TrG5h" value="ProjectReference" />
  </node>
  <node concept="356sEV" id="4ejLsrQzrBY">
    <property role="TrG5h" value="SomeExtraFile" />
    <property role="3Le9LX" value=".cs" />
    <node concept="356WMU" id="4ejLsrQzrBZ" role="356KY_">
      <node concept="356sEK" id="4ejLsrQzrCA" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQzrCB" role="356sEH">
          <property role="TrG5h" value="usings" />
        </node>
        <node concept="2EixSi" id="4ejLsrQzrCC" role="2EinRH" />
        <node concept="2PVmq6" id="4ejLsrQzrCQ" role="lGtFl">
          <property role="2P6uU2" value="4ejLsrQyyn4/Root" />
          <ref role="2PVmq4" node="4ejLsrQwK2t" resolve="C# using" />
        </node>
      </node>
      <node concept="356sEK" id="4ejLsrQzrC0" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQzrC1" role="356sEH">
          <property role="TrG5h" value="hello" />
          <node concept="2PVmq1" id="4ejLsrQzrCj" role="lGtFl">
            <node concept="2PVmLD" id="4ejLsrQzrCt" role="2PVmLZ">
              <ref role="2PVmLQ" node="4ejLsrQzrCl" resolve="Linq" />
            </node>
            <node concept="2PVmLD" id="4ejLsrQzsGp" role="2PVmLZ">
              <ref role="2PVmLQ" node="4ejLsrQzsGh" resolve="SomeRef" />
            </node>
          </node>
        </node>
        <node concept="2EixSi" id="4ejLsrQzrC2" role="2EinRH" />
      </node>
      <node concept="356sEK" id="4ejLsrQzrCa" role="383Ya9">
        <node concept="356sEF" id="4ejLsrQzrCb" role="356sEH">
          <property role="TrG5h" value="blub" />
          <node concept="2PVmq1" id="4ejLsrQzrCy" role="lGtFl">
            <node concept="2PVmLD" id="4ejLsrQzrC$" role="2PVmLZ">
              <ref role="2PVmLQ" node="4ejLsrQwK2u" resolve="Shielded" />
            </node>
          </node>
        </node>
        <node concept="2EixSi" id="4ejLsrQzrCc" role="2EinRH" />
      </node>
    </node>
  </node>
  <node concept="2PVmre" id="4ejLsrQzrCl">
    <property role="TrG5h" value="Linq" />
    <ref role="2PVmq2" node="4ejLsrQwK2t" resolve="C# using" />
    <node concept="356sEK" id="4ejLsrQzrCn" role="2PVmrf">
      <node concept="356sEF" id="4ejLsrQzrCo" role="356sEH">
        <property role="TrG5h" value="using system.collections.linq;" />
      </node>
      <node concept="2EixSi" id="4ejLsrQzrCp" role="2EinRH" />
    </node>
  </node>
  <node concept="2PVmre" id="4ejLsrQzsGh">
    <property role="TrG5h" value="SomeRef" />
    <ref role="2PVmq2" node="4ejLsrQyyn1" resolve="ProjectReference" />
    <node concept="356sEK" id="4ejLsrQzsGj" role="2PVmrf">
      <node concept="356sEF" id="4ejLsrQzsGk" role="356sEH">
        <property role="TrG5h" value="projectRef blub" />
      </node>
      <node concept="2EixSi" id="4ejLsrQzsGl" role="2EinRH" />
    </node>
  </node>
</model>

