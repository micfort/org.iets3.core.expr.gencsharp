<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3336a6ad-b9e8-4066-9962-09ed53e0248a(org.iets3.core.expr.gencsharp.tests.generator.templates@generator)">
  <persistence version="9" />
  <languages>
    <use id="cf681fc9-c798-4f89-af38-ba3c0ac342d9" name="com.dslfoundry.plaintextflow" version="0" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="4" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="hsl7" ref="r:0f7f49fa-f0fb-4878-a29b-404e6201a137(org.iets3.core.expr.gencsharp.tests.structure)" />
    <import index="av4b" ref="r:ba7faab6-2b80-43d5-8b95-0c440665312c(org.iets3.core.expr.tests.structure)" />
    <import index="vs0r" ref="r:f7764ca4-8c75-4049-922b-08516400a727(com.mbeddr.core.base.structure)" />
    <import index="g1u8" ref="r:7c9cc190-d289-4193-8ba6-e68d1d5bc3de(org.iets3.core.expr.gencsharp.base.generator.templates@generator)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tp5o" ref="r:00000000-0000-4000-0000-011c89590380(jetbrains.mps.lang.test.behavior)" implicit="true" />
    <import index="4kwy" ref="r:657c9fde-2f36-4e61-ae17-20f02b8630ad(org.iets3.core.base.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271369338" name="jetbrains.mps.baseLanguage.structure.IsEmptyOperation" flags="nn" index="17RlXB" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114706874351" name="jetbrains.mps.lang.generator.structure.CopySrcNodeMacro" flags="ln" index="29HgVG">
        <child id="1168024447342" name="sourceNodeQuery" index="3NFExx" />
      </concept>
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
        <child id="1167514678247" name="rootMappingRule" index="3lj3bC" />
      </concept>
      <concept id="1177093525992" name="jetbrains.mps.lang.generator.structure.InlineTemplate_RuleConsequence" flags="lg" index="gft3U">
        <child id="1177093586806" name="templateNode" index="gfFT$" />
      </concept>
      <concept id="1112730859144" name="jetbrains.mps.lang.generator.structure.TemplateSwitch" flags="ig" index="jVnub">
        <reference id="1112820671508" name="modifiedSwitch" index="phYkn" />
        <child id="1167340453568" name="reductionMappingRule" index="3aUrZf" />
      </concept>
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167168920554" name="jetbrains.mps.lang.generator.structure.BaseMappingRule_Condition" flags="in" index="30G5F_" />
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <property id="1167272244852" name="applyToConceptInheritors" index="36QftV" />
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
        <child id="1167169362365" name="conditionFunction" index="30HLyM" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167514355419" name="jetbrains.mps.lang.generator.structure.Root_MappingRule" flags="lg" index="3lhOvk">
        <reference id="1167514355421" name="template" index="3lhOvi" />
      </concept>
      <concept id="982871510068000147" name="jetbrains.mps.lang.generator.structure.TemplateSwitchMacro" flags="lg" index="1sPUBX" />
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167945743726" name="jetbrains.mps.lang.generator.structure.IfMacro_Condition" flags="in" index="3IZrLx" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1168024337012" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodeQuery" flags="in" index="3NFfHV" />
      <concept id="1118773211870" name="jetbrains.mps.lang.generator.structure.IfMacro" flags="ln" index="1W57fq">
        <child id="1167945861827" name="conditionFunction" index="3IZSJc" />
      </concept>
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1229477454423" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_GetOriginalCopiedInputByOutput" flags="nn" index="12$id9">
        <child id="1229477520175" name="outputNode" index="12$y8L" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="990507d3-3527-4c54-bfe9-0ca3c9c6247a" name="com.dslfoundry.plaintextgen">
      <concept id="5082088080656902716" name="com.dslfoundry.plaintextgen.structure.NewlineMarker" flags="ng" index="2EixSi" />
      <concept id="1145195647825954804" name="com.dslfoundry.plaintextgen.structure.word" flags="ng" index="356sEF" />
      <concept id="1145195647825954799" name="com.dslfoundry.plaintextgen.structure.Line" flags="ng" index="356sEK">
        <child id="5082088080656976323" name="newlineMarker" index="2EinRH" />
        <child id="1145195647825954802" name="words" index="356sEH" />
      </concept>
      <concept id="1145195647825954793" name="com.dslfoundry.plaintextgen.structure.SpaceIndentedText" flags="ng" index="356sEQ">
        <property id="5198309202558919052" name="indent" index="333NGx" />
      </concept>
      <concept id="1145195647825954788" name="com.dslfoundry.plaintextgen.structure.TextgenText" flags="ng" index="356sEV">
        <property id="5407518469085446424" name="ext" index="3Le9LX" />
        <child id="1145195647826100986" name="content" index="356KY_" />
      </concept>
      <concept id="1145195647826084325" name="com.dslfoundry.plaintextgen.structure.VerticalLines" flags="ng" index="356WMU" />
      <concept id="7214912913997260680" name="com.dslfoundry.plaintextgen.structure.IVerticalGroup" flags="ng" index="383Yap">
        <child id="7214912913997260696" name="lines" index="383Ya9" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1143234257716" name="jetbrains.mps.lang.smodel.structure.Node_GetModelOperation" flags="nn" index="I4A8Y" />
      <concept id="1212008292747" name="jetbrains.mps.lang.smodel.structure.Model_GetLongNameOperation" flags="nn" index="LkI2h" />
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="bUwia" id="2Y7ZIgggsn0">
    <property role="TrG5h" value="main" />
    <node concept="3aamgX" id="26UovjNYx2z" role="3acgRq">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:78hTg1$THIw" resolve="AbstractTestItem" />
      <node concept="gft3U" id="26UovjNYyFD" role="1lVwrX">
        <node concept="356sEF" id="26UovjNYyG_" role="gfFT$">
          <property role="TrG5h" value="assert" />
          <node concept="1sPUBX" id="26UovjNYyGJ" role="lGtFl">
            <ref role="v9R2y" node="26UovjNYyGB" resolve="switchTestItem" />
          </node>
        </node>
      </node>
      <node concept="30G5F_" id="26UovjNYx4o" role="30HLyM">
        <node concept="3clFbS" id="26UovjNYx4p" role="2VODD2">
          <node concept="3clFbF" id="26UovjNYx8o" role="3cqZAp">
            <node concept="3fqX7Q" id="26UovjNYyEB" role="3clFbG">
              <node concept="2OqwBi" id="26UovjNYyED" role="3fr31v">
                <node concept="30H73N" id="26UovjNYyEE" role="2Oq$k0" />
                <node concept="1mIQ4w" id="26UovjNYyEF" role="2OqNvi">
                  <node concept="chp4Y" id="26UovjNYyEG" role="cj9EA">
                    <ref role="cht4Q" to="av4b:3BFGe1EJa4q" resolve="VectorTestItem" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNY3cn" role="3acgRq">
      <ref role="30HIoZ" to="av4b:ub9nkyHAb7" resolve="TestCase" />
      <node concept="gft3U" id="26UovjNY3cr" role="1lVwrX">
        <node concept="356WMU" id="26UovjNYei6" role="gfFT$">
          <node concept="356sEK" id="26UovjNYei7" role="383Ya9">
            <node concept="356sEF" id="26UovjNYei8" role="356sEH">
              <property role="TrG5h" value="public class " />
            </node>
            <node concept="356sEF" id="26UovjNYem4" role="356sEH">
              <property role="TrG5h" value="testcasename" />
              <node concept="17Uvod" id="26UovjNYem7" role="lGtFl">
                <property role="2qtEX9" value="name" />
                <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                <node concept="3zFVjK" id="26UovjNYema" role="3zH0cK">
                  <node concept="3clFbS" id="26UovjNYemb" role="2VODD2">
                    <node concept="3clFbF" id="26UovjNYemh" role="3cqZAp">
                      <node concept="2OqwBi" id="26UovjNYemc" role="3clFbG">
                        <node concept="3TrcHB" id="26UovjNYemf" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                        <node concept="30H73N" id="26UovjNYemg" role="2Oq$k0" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2EixSi" id="26UovjNYeia" role="2EinRH" />
          </node>
          <node concept="356sEK" id="26UovjNYeib" role="383Ya9">
            <node concept="356sEF" id="26UovjNYeic" role="356sEH">
              <property role="TrG5h" value="{" />
            </node>
            <node concept="2EixSi" id="26UovjNYeie" role="2EinRH" />
          </node>
          <node concept="356sEQ" id="7W7a84sNkVw" role="383Ya9">
            <property role="333NGx" value="  " />
            <node concept="356sEK" id="7W7a84sNkx7" role="383Ya9">
              <node concept="356sEF" id="7W7a84sNkx8" role="356sEH">
                <property role="TrG5h" value="private readonly ITestOutputHelper output;" />
              </node>
              <node concept="2EixSi" id="7W7a84sNkxa" role="2EinRH" />
            </node>
            <node concept="356sEK" id="7W7a84sNkxb" role="383Ya9">
              <node concept="2EixSi" id="7W7a84sNkxe" role="2EinRH" />
            </node>
            <node concept="356sEK" id="7W7a84sNkxf" role="383Ya9">
              <node concept="356sEF" id="7W7a84sNkxg" role="356sEH">
                <property role="TrG5h" value="public " />
              </node>
              <node concept="356sEF" id="7W7a84sNl0k" role="356sEH">
                <property role="TrG5h" value="name" />
                <node concept="17Uvod" id="7W7a84sNl0l" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="7W7a84sNl0m" role="3zH0cK">
                    <node concept="3clFbS" id="7W7a84sNl0n" role="2VODD2">
                      <node concept="3clFbF" id="7W7a84sNl0o" role="3cqZAp">
                        <node concept="2OqwBi" id="7W7a84sNl0p" role="3clFbG">
                          <node concept="3TrcHB" id="7W7a84sNl0q" role="2OqNvi">
                            <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                          </node>
                          <node concept="30H73N" id="7W7a84sNl0r" role="2Oq$k0" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="356sEF" id="7W7a84sNl0g" role="356sEH">
                <property role="TrG5h" value="(ITestOutputHelper output)" />
              </node>
              <node concept="2EixSi" id="7W7a84sNkxi" role="2EinRH" />
            </node>
            <node concept="356sEK" id="7W7a84sNkxj" role="383Ya9">
              <node concept="356sEF" id="7W7a84sNkxk" role="356sEH">
                <property role="TrG5h" value="{" />
              </node>
              <node concept="2EixSi" id="7W7a84sNkxm" role="2EinRH" />
            </node>
            <node concept="356sEQ" id="7W7a84sNkxr" role="383Ya9">
              <property role="333NGx" value="    " />
              <node concept="356sEK" id="7W7a84sNkxn" role="383Ya9">
                <node concept="356sEF" id="7W7a84sNkxo" role="356sEH">
                  <property role="TrG5h" value="this.output = output;" />
                </node>
                <node concept="2EixSi" id="7W7a84sNkxq" role="2EinRH" />
              </node>
            </node>
            <node concept="356sEK" id="7W7a84sNkxs" role="383Ya9">
              <node concept="356sEF" id="7W7a84sNkxt" role="356sEH">
                <property role="TrG5h" value="}" />
              </node>
              <node concept="2EixSi" id="7W7a84sNkxv" role="2EinRH" />
            </node>
          </node>
          <node concept="356sEK" id="7W7a84sNGOH" role="383Ya9">
            <node concept="2EixSi" id="7W7a84sNGOJ" role="2EinRH" />
          </node>
          <node concept="356sEQ" id="26UovjNYeij" role="383Ya9">
            <property role="333NGx" value="  " />
            <node concept="356sEK" id="26UovjNYeif" role="383Ya9">
              <node concept="356sEF" id="26UovjNYeig" role="356sEH">
                <property role="TrG5h" value="[Fact]" />
              </node>
              <node concept="2EixSi" id="26UovjNYeii" role="2EinRH" />
            </node>
            <node concept="356sEK" id="26UovjNYeik" role="383Ya9">
              <node concept="356sEF" id="26UovjNYeil" role="356sEH">
                <property role="TrG5h" value="public void " />
              </node>
              <node concept="356sEF" id="26UovjNYgNm" role="356sEH">
                <property role="TrG5h" value="assertTestName" />
                <node concept="17Uvod" id="26UovjNYgNW" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="26UovjNYgNX" role="3zH0cK">
                    <node concept="3clFbS" id="26UovjNYgNY" role="2VODD2">
                      <node concept="3clFbF" id="26UovjNYgSD" role="3cqZAp">
                        <node concept="2OqwBi" id="26UovjNYhk4" role="3clFbG">
                          <node concept="30H73N" id="26UovjNYgSC" role="2Oq$k0" />
                          <node concept="2qgKlT" id="26UovjO0im_" role="2OqNvi">
                            <ref role="37wK5l" to="tp5o:hHOMYE$" resolve="getName" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="356sEF" id="26UovjNYgNn" role="356sEH">
                <property role="TrG5h" value="()" />
              </node>
              <node concept="2EixSi" id="26UovjNYein" role="2EinRH" />
            </node>
            <node concept="356sEK" id="26UovjNYeio" role="383Ya9">
              <node concept="356sEF" id="26UovjNYeip" role="356sEH">
                <property role="TrG5h" value="{" />
              </node>
              <node concept="2EixSi" id="26UovjNYeir" role="2EinRH" />
            </node>
            <node concept="356sEQ" id="26UovjNYeiw" role="383Ya9">
              <property role="333NGx" value="  " />
              <node concept="356sEK" id="26UovjNYeis" role="383Ya9">
                <node concept="2EixSi" id="26UovjNYeiv" role="2EinRH" />
                <node concept="29HgVG" id="26UovjNYx2w" role="lGtFl">
                  <node concept="3NFfHV" id="26UovjNZpzI" role="3NFExx">
                    <node concept="3clFbS" id="26UovjNZpzJ" role="2VODD2">
                      <node concept="3clFbF" id="26UovjNZp_O" role="3cqZAp">
                        <node concept="30H73N" id="26UovjNZp_N" role="3clFbG" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="356sEK" id="26UovjNYeix" role="383Ya9">
              <node concept="356sEF" id="26UovjNYeiy" role="356sEH">
                <property role="TrG5h" value="}" />
              </node>
              <node concept="2EixSi" id="26UovjNYei$" role="2EinRH" />
            </node>
            <node concept="1WS0z7" id="26UovjNYej8" role="lGtFl">
              <node concept="3JmXsc" id="26UovjNYej9" role="3Jn$fo">
                <node concept="3clFbS" id="26UovjNYeja" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNYex$" role="3cqZAp">
                    <node concept="2OqwBi" id="1Ds3skyz0Dd" role="3clFbG">
                      <node concept="2OqwBi" id="1Ds3skyyUOE" role="2Oq$k0">
                        <node concept="30H73N" id="1Ds3skyyUsI" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="1Ds3skyyVoP" role="2OqNvi">
                          <ref role="3TtcxE" to="av4b:ub9nkyHAcK" resolve="items" />
                        </node>
                      </node>
                      <node concept="3zZkjj" id="1Ds3skyz4KX" role="2OqNvi">
                        <node concept="1bVj0M" id="1Ds3skyz4KZ" role="23t8la">
                          <node concept="3clFbS" id="1Ds3skyz4L0" role="1bW5cS">
                            <node concept="3clFbF" id="1Ds3skyz5bf" role="3cqZAp">
                              <node concept="3fqX7Q" id="1Ds3skyz5bd" role="3clFbG">
                                <node concept="2OqwBi" id="1Ds3skyz5SZ" role="3fr31v">
                                  <node concept="37vLTw" id="1Ds3skyz5bu" role="2Oq$k0">
                                    <ref role="3cqZAo" node="1Ds3skyz4L1" resolve="it" />
                                  </node>
                                  <node concept="1mIQ4w" id="1Ds3skyz75X" role="2OqNvi">
                                    <node concept="chp4Y" id="1Ds3skyz7zb" role="cj9EA">
                                      <ref role="cht4Q" to="vs0r:Ug1QzfhXN3" resolve="IEmpty" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="1Ds3skyz4L1" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="1Ds3skyz4L2" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEK" id="26UovjNYei_" role="383Ya9">
            <node concept="356sEF" id="26UovjNYeiA" role="356sEH">
              <property role="TrG5h" value="}" />
            </node>
            <node concept="2EixSi" id="26UovjNYeiC" role="2EinRH" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sLZ4V" role="3acgRq">
      <ref role="30HIoZ" to="av4b:1ESmOTbCHcU" resolve="AndMatcher" />
      <node concept="gft3U" id="7W7a84sLZmO" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sLZmU" role="gfFT$">
          <node concept="356sEF" id="7W7a84sLZmV" role="356sEH">
            <property role="TrG5h" value="A" />
            <node concept="29HgVG" id="7W7a84sLZsR" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sLZsS" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sLZsT" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sLZsZ" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sLZsU" role="3clFbG">
                      <node concept="3TrEf2" id="7W7a84sLZsX" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:1ESmOTbCHdR" resolve="left" />
                      </node>
                      <node concept="30H73N" id="7W7a84sLZsY" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sLZn4" role="356sEH">
            <property role="TrG5h" value=" &amp;&amp; " />
          </node>
          <node concept="356sEF" id="7W7a84sLZn7" role="356sEH">
            <property role="TrG5h" value="B" />
            <node concept="29HgVG" id="7W7a84sLZnc" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sLZnd" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sLZne" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sLZnk" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sLZnf" role="3clFbG">
                      <node concept="3TrEf2" id="7W7a84sLZni" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:1ESmOTbCHdT" resolve="right" />
                      </node>
                      <node concept="30H73N" id="7W7a84sLZnj" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2EixSi" id="7W7a84sLZmW" role="2EinRH" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sLZtN" role="3acgRq">
      <ref role="30HIoZ" to="av4b:5Pgo_ASrZfv" resolve="ContainsString" />
      <node concept="gft3U" id="7W7a84sLZYb" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sLZYh" role="gfFT$">
          <node concept="356sEF" id="7W7a84sLZYi" role="356sEH">
            <property role="TrG5h" value="value" />
            <node concept="29HgVG" id="7W7a84sM04n" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sM04o" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sM04p" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sM07q" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sM0X2" role="3clFbG">
                      <node concept="2OqwBi" id="7W7a84sM0iW" role="2Oq$k0">
                        <node concept="30H73N" id="7W7a84sM07p" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="7W7a84sM0u1" role="2OqNvi">
                          <node concept="1xMEDy" id="7W7a84sM0u3" role="1xVPHs">
                            <node concept="chp4Y" id="7W7a84sM0$P" role="ri$Ld">
                              <ref role="cht4Q" to="av4b:5Pgo_AS3Joq" resolve="AssertThatTestItem" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="7W7a84sM14S" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:5Pgo_AS3PT3" resolve="value" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sLZYn" role="356sEH">
            <property role="TrG5h" value=".Contains(" />
          </node>
          <node concept="356sEF" id="7W7a84sLZYq" role="356sEH">
            <property role="TrG5h" value="matcher" />
            <node concept="29HgVG" id="7W7a84sLZYE" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sLZYF" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sLZYG" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sLZYM" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sLZYH" role="3clFbG">
                      <node concept="3TrEf2" id="7W7a84sLZYK" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:5Pgo_ASrZg1" resolve="text" />
                      </node>
                      <node concept="30H73N" id="7W7a84sLZYL" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sLZYz" role="356sEH">
            <property role="TrG5h" value=")" />
          </node>
          <node concept="2EixSi" id="7W7a84sLZYj" role="2EinRH" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sM1h3" role="3acgRq">
      <ref role="30HIoZ" to="av4b:5Pgo_ASbFvn" resolve="IsInvalid" />
      <node concept="gft3U" id="7W7a84sM1h4" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sM1$V" role="gfFT$">
          <node concept="356sEF" id="7W7a84sM1$W" role="356sEH">
            <property role="TrG5h" value="new Func&lt;bool&gt;(() =&gt; { try { " />
          </node>
          <node concept="356sEF" id="7W7a84sM1_i" role="356sEH">
            <property role="TrG5h" value="value" />
            <node concept="29HgVG" id="7W7a84sM1_j" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sM1_k" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sM1_l" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sM1_m" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sM1_n" role="3clFbG">
                      <node concept="2OqwBi" id="7W7a84sM1_o" role="2Oq$k0">
                        <node concept="30H73N" id="7W7a84sM1_p" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="7W7a84sM1_q" role="2OqNvi">
                          <node concept="1xMEDy" id="7W7a84sM1_r" role="1xVPHs">
                            <node concept="chp4Y" id="7W7a84sM1_s" role="ri$Ld">
                              <ref role="cht4Q" to="av4b:5Pgo_AS3Joq" resolve="AssertThatTestItem" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="7W7a84sM1_t" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:5Pgo_AS3PT3" resolve="value" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sM1_4" role="356sEH">
            <property role="TrG5h" value="; return false; } catch { return true; }})()" />
          </node>
          <node concept="2EixSi" id="7W7a84sM1$X" role="2EinRH" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sM1Li" role="3acgRq">
      <ref role="30HIoZ" to="av4b:5Pgo_ASae6g" resolve="IsValidRecord" />
      <node concept="gft3U" id="7W7a84sM2iq" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sM2iw" role="gfFT$">
          <node concept="356sEF" id="7W7a84sM5pr" role="356sEH">
            <property role="TrG5h" value="value" />
            <node concept="29HgVG" id="7W7a84sM5ps" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sM5pt" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sM5pu" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sM5pv" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sM5pw" role="3clFbG">
                      <node concept="2OqwBi" id="7W7a84sM5px" role="2Oq$k0">
                        <node concept="30H73N" id="7W7a84sM5py" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="7W7a84sM5pz" role="2OqNvi">
                          <node concept="1xMEDy" id="7W7a84sM5p$" role="1xVPHs">
                            <node concept="chp4Y" id="7W7a84sM5p_" role="ri$Ld">
                              <ref role="cht4Q" to="av4b:5Pgo_AS3Joq" resolve="AssertThatTestItem" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="7W7a84sM5pA" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:5Pgo_AS3PT3" resolve="value" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sM2iA" role="356sEH">
            <property role="TrG5h" value=" is not null" />
          </node>
          <node concept="2EixSi" id="7W7a84sM2iy" role="2EinRH" />
        </node>
      </node>
    </node>
    <node concept="3lhOvk" id="26UovjNXSS9" role="3lj3bC">
      <ref role="30HIoZ" to="av4b:ub9nkyK62f" resolve="TestSuite" />
      <ref role="3lhOvi" node="26UovjNXVgx" resolve="map_TestSuite" />
    </node>
  </node>
  <node concept="356sEV" id="26UovjNXVgx">
    <property role="TrG5h" value="name" />
    <property role="3Le9LX" value=".cs" />
    <node concept="356WMU" id="26UovjNY0Kp" role="356KY_">
      <node concept="356sEK" id="26UovjNY0Kq" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0Kr" role="356sEH">
          <property role="TrG5h" value="using System;" />
        </node>
        <node concept="2EixSi" id="26UovjNY0Kt" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0Ku" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0Kv" role="356sEH">
          <property role="TrG5h" value="using System.Collections.Immutable;" />
        </node>
        <node concept="2EixSi" id="26UovjNY0Kx" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0Ky" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0Kz" role="356sEH">
          <property role="TrG5h" value="using KernelFRuntime;" />
        </node>
        <node concept="2EixSi" id="26UovjNY0K_" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KA" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0KB" role="356sEH">
          <property role="TrG5h" value="using Xunit;" />
        </node>
        <node concept="2EixSi" id="26UovjNY0KD" role="2EinRH" />
      </node>
      <node concept="356sEK" id="7W7a84sNl_a" role="383Ya9">
        <node concept="356sEF" id="7W7a84sNl_b" role="356sEH">
          <property role="TrG5h" value="using Xunit.Abstractions;" />
        </node>
        <node concept="2EixSi" id="7W7a84sNl_c" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KE" role="383Ya9">
        <node concept="2EixSi" id="26UovjNY0KH" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KI" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0KJ" role="356sEH">
          <property role="TrG5h" value="namespace " />
        </node>
        <node concept="356sEF" id="26UovjNY1l7" role="356sEH">
          <property role="TrG5h" value="SandBoxTesting" />
          <node concept="17Uvod" id="26UovjNY1lc" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="26UovjNY1ld" role="3zH0cK">
              <node concept="3clFbS" id="26UovjNY1le" role="2VODD2">
                <node concept="3clFbJ" id="1qqzbvYhQdW" role="3cqZAp">
                  <node concept="3clFbS" id="1qqzbvYhQdY" role="3clFbx">
                    <node concept="3cpWs6" id="1qqzbvYhRJS" role="3cqZAp">
                      <node concept="2OqwBi" id="1qqzbvYhS9z" role="3cqZAk">
                        <node concept="2OqwBi" id="1qqzbvYhRYn" role="2Oq$k0">
                          <node concept="30H73N" id="1qqzbvYhRPb" role="2Oq$k0" />
                          <node concept="I4A8Y" id="1qqzbvYhS1P" role="2OqNvi" />
                        </node>
                        <node concept="LkI2h" id="1qqzbvYhSfz" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="1qqzbvYhREB" role="3clFbw">
                    <node concept="2OqwBi" id="1qqzbvYhQJ2" role="2Oq$k0">
                      <node concept="30H73N" id="1qqzbvYhQjk" role="2Oq$k0" />
                      <node concept="3TrcHB" id="1qqzbvYhQM$" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:hnGE5uv" resolve="virtualPackage" />
                      </node>
                    </node>
                    <node concept="17RlXB" id="1qqzbvYhRH2" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3clFbF" id="1qqzbvYhSmd" role="3cqZAp">
                  <node concept="3cpWs3" id="1qqzbvYjxv8" role="3clFbG">
                    <node concept="2OqwBi" id="1qqzbvYhWqf" role="3uHU7w">
                      <node concept="30H73N" id="1qqzbvYhWcf" role="2Oq$k0" />
                      <node concept="3TrcHB" id="1qqzbvYhWyL" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:hnGE5uv" resolve="virtualPackage" />
                      </node>
                    </node>
                    <node concept="3cpWs3" id="1qqzbvYhWaA" role="3uHU7B">
                      <node concept="2OqwBi" id="1qqzbvYhVtn" role="3uHU7B">
                        <node concept="2OqwBi" id="1qqzbvYhV5L" role="2Oq$k0">
                          <node concept="30H73N" id="1qqzbvYhSmc" role="2Oq$k0" />
                          <node concept="I4A8Y" id="1qqzbvYhVc6" role="2OqNvi" />
                        </node>
                        <node concept="LkI2h" id="1qqzbvYhVFg" role="2OqNvi" />
                      </node>
                      <node concept="Xl_RD" id="1qqzbvYjx_r" role="3uHU7w">
                        <property role="Xl_RC" value="." />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="356sEF" id="26UovjNY1l8" role="356sEH">
          <property role="TrG5h" value=";" />
        </node>
        <node concept="2EixSi" id="26UovjNY0KL" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KM" role="383Ya9">
        <node concept="2EixSi" id="26UovjNY0KP" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KQ" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0KR" role="356sEH">
          <property role="TrG5h" value="public class " />
        </node>
        <node concept="356sEF" id="26UovjNY0Vf" role="356sEH">
          <property role="TrG5h" value="name" />
          <node concept="17Uvod" id="26UovjNY0Vi" role="lGtFl">
            <property role="2qtEX9" value="name" />
            <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
            <node concept="3zFVjK" id="26UovjNY0Vl" role="3zH0cK">
              <node concept="3clFbS" id="26UovjNY0Vm" role="2VODD2">
                <node concept="3clFbF" id="26UovjNY0Vs" role="3cqZAp">
                  <node concept="2OqwBi" id="26UovjNY0Vn" role="3clFbG">
                    <node concept="3TrcHB" id="26UovjNY0Vq" role="2OqNvi">
                      <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                    </node>
                    <node concept="30H73N" id="26UovjNY0Vr" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2EixSi" id="26UovjNY0KT" role="2EinRH" />
      </node>
      <node concept="356sEK" id="26UovjNY0KU" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0KV" role="356sEH">
          <property role="TrG5h" value="{" />
        </node>
        <node concept="2EixSi" id="26UovjNY0KX" role="2EinRH" />
      </node>
      <node concept="356sEQ" id="26UovjNY0L2" role="383Ya9">
        <property role="333NGx" value="  " />
        <node concept="356sEK" id="26UovjNY0LX" role="383Ya9">
          <node concept="2EixSi" id="26UovjNY0LZ" role="2EinRH" />
          <node concept="2b32R4" id="26UovjNYj5a" role="lGtFl">
            <node concept="3JmXsc" id="26UovjNYj5d" role="2P8S$">
              <node concept="3clFbS" id="26UovjNYj5e" role="2VODD2">
                <node concept="3clFbF" id="26UovjNYj5k" role="3cqZAp">
                  <node concept="2OqwBi" id="26UovjNYj5f" role="3clFbG">
                    <node concept="3Tsc0h" id="26UovjNYj5i" role="2OqNvi">
                      <ref role="3TtcxE" to="av4b:ub9nkyK62i" resolve="contents" />
                    </node>
                    <node concept="30H73N" id="26UovjNYj5j" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="356sEK" id="26UovjNY0Lf" role="383Ya9">
        <node concept="356sEF" id="26UovjNY0Lg" role="356sEH">
          <property role="TrG5h" value="}" />
        </node>
        <node concept="2EixSi" id="26UovjNY0Li" role="2EinRH" />
      </node>
    </node>
    <node concept="n94m4" id="26UovjNXVgz" role="lGtFl">
      <ref role="n9lRv" to="av4b:ub9nkyK62f" resolve="TestSuite" />
    </node>
    <node concept="17Uvod" id="26UovjNYqWe" role="lGtFl">
      <property role="2qtEX9" value="name" />
      <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
      <node concept="3zFVjK" id="26UovjNYqWf" role="3zH0cK">
        <node concept="3clFbS" id="26UovjNYqWg" role="2VODD2">
          <node concept="3clFbF" id="26UovjNYqZN" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjNYrpZ" role="3clFbG">
              <node concept="30H73N" id="26UovjNYqZM" role="2Oq$k0" />
              <node concept="3TrcHB" id="26UovjNYs34" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="jVnub" id="26UovjNYyGB">
    <property role="TrG5h" value="SwitchTestItem" />
    <node concept="3aamgX" id="26UovjNYYLw" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:ub9nkyHAba" resolve="AssertTestItem" />
      <node concept="gft3U" id="26UovjNZ1Or" role="1lVwrX">
        <node concept="356sEK" id="26UovjNZ1Pl" role="gfFT$">
          <node concept="356sEF" id="26UovjNZ1Pm" role="356sEH">
            <property role="TrG5h" value="Assert.Equal(" />
          </node>
          <node concept="356sEF" id="26UovjNZ1Tw" role="356sEH">
            <property role="TrG5h" value="expected" />
            <node concept="29HgVG" id="26UovjNZ21Z" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ220" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ221" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ227" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ222" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ225" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbd" resolve="expected" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ226" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ1Tz" role="356sEH">
            <property role="TrG5h" value=", " />
          </node>
          <node concept="356sEF" id="26UovjNZ1TB" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjNZ1TN" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ1TO" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ1TP" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ1TV" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ1TQ" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ1TT" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbb" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ1TU" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ1TG" role="356sEH">
            <property role="TrG5h" value=");" />
          </node>
          <node concept="2EixSi" id="26UovjNZ1Pn" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjNYYL$" role="30HLyM">
        <node concept="3clFbS" id="26UovjNYYL_" role="2VODD2">
          <node concept="3clFbF" id="26UovjNYYP$" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjNZ1lp" role="3clFbG">
              <node concept="2OqwBi" id="26UovjNYZll" role="2Oq$k0">
                <node concept="30H73N" id="26UovjNYYPz" role="2Oq$k0" />
                <node concept="3TrEf2" id="26UovjNZ05B" role="2OqNvi">
                  <ref role="3Tt5mk" to="av4b:ub9nkyHAbI" resolve="op" />
                </node>
              </node>
              <node concept="1mIQ4w" id="26UovjNZ1mX" role="2OqNvi">
                <node concept="chp4Y" id="26UovjNZ1_H" role="cj9EA">
                  <ref role="cht4Q" to="av4b:ub9nkyHAbh" resolve="EqualsTestOp" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNZ25c" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:ub9nkyHAba" resolve="AssertTestItem" />
      <node concept="gft3U" id="26UovjNZ25d" role="1lVwrX">
        <node concept="356sEK" id="26UovjNZ25e" role="gfFT$">
          <node concept="356sEF" id="26UovjNZ25f" role="356sEH">
            <property role="TrG5h" value="Assert.NotEqual(" />
          </node>
          <node concept="356sEF" id="26UovjNZ25g" role="356sEH">
            <property role="TrG5h" value="expected" />
            <node concept="29HgVG" id="26UovjNZ25h" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ25i" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ25j" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ25k" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ25l" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ25m" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbd" resolve="expected" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ25n" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ25o" role="356sEH">
            <property role="TrG5h" value=", " />
          </node>
          <node concept="356sEF" id="26UovjNZ25p" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjNZ25q" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ25r" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ25s" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ25t" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ25u" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ25v" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbb" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ25w" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ25x" role="356sEH">
            <property role="TrG5h" value=");" />
          </node>
          <node concept="2EixSi" id="26UovjNZ25y" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjNZ25z" role="30HLyM">
        <node concept="3clFbS" id="26UovjNZ25$" role="2VODD2">
          <node concept="3clFbF" id="26UovjNZ25_" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjNZ25A" role="3clFbG">
              <node concept="2OqwBi" id="26UovjNZ25B" role="2Oq$k0">
                <node concept="30H73N" id="26UovjNZ25C" role="2Oq$k0" />
                <node concept="3TrEf2" id="26UovjNZ25D" role="2OqNvi">
                  <ref role="3Tt5mk" to="av4b:ub9nkyHAbI" resolve="op" />
                </node>
              </node>
              <node concept="1mIQ4w" id="26UovjNZ25E" role="2OqNvi">
                <node concept="chp4Y" id="26UovjNZ25F" role="cj9EA">
                  <ref role="cht4Q" to="av4b:1EZBwZ4mn8x" resolve="NotEqualsTestOp" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNZ2Gh" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:ub9nkyHAba" resolve="AssertTestItem" />
      <node concept="gft3U" id="26UovjNZ2Gi" role="1lVwrX">
        <node concept="356sEK" id="26UovjNZ2Gj" role="gfFT$">
          <node concept="356sEF" id="26UovjNZ2Gk" role="356sEH">
            <property role="TrG5h" value="Assert.Equal(" />
          </node>
          <node concept="356sEF" id="26UovjNZ2Gl" role="356sEH">
            <property role="TrG5h" value="expected" />
            <node concept="29HgVG" id="26UovjNZ2Gm" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ2Gn" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ2Go" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ2Gp" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ2Gq" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ2Gr" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbd" resolve="expected" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ2Gs" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ2Gt" role="356sEH">
            <property role="TrG5h" value=", " />
          </node>
          <node concept="356sEF" id="26UovjNZ2Gu" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjNZ2Gv" role="lGtFl">
              <node concept="3NFfHV" id="26UovjNZ2Gw" role="3NFExx">
                <node concept="3clFbS" id="26UovjNZ2Gx" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ2Gy" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjNZ2Gz" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjNZ2G$" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:ub9nkyHAbb" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjNZ2G_" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ2GA" role="356sEH">
            <property role="TrG5h" value=", " />
          </node>
          <node concept="356sEF" id="26UovjNZ3t5" role="356sEH">
            <property role="TrG5h" value="precision" />
            <node concept="17Uvod" id="26UovjNZ3wl" role="lGtFl">
              <property role="2qtEX9" value="name" />
              <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
              <node concept="3zFVjK" id="26UovjNZ3wm" role="3zH0cK">
                <node concept="3clFbS" id="26UovjNZ3wn" role="2VODD2">
                  <node concept="3clFbF" id="26UovjNZ3_2" role="3cqZAp">
                    <node concept="3cpWs3" id="26UovjNZ7Sj" role="3clFbG">
                      <node concept="Xl_RD" id="26UovjNZ7Sn" role="3uHU7w">
                        <property role="Xl_RC" value="" />
                      </node>
                      <node concept="2OqwBi" id="26UovjNZ6mJ" role="3uHU7B">
                        <node concept="1PxgMI" id="26UovjNZ66F" role="2Oq$k0">
                          <node concept="chp4Y" id="26UovjNZ67P" role="3oSUPX">
                            <ref role="cht4Q" to="av4b:4kV9Ob9XpO0" resolve="RealEqualsTestOp" />
                          </node>
                          <node concept="2OqwBi" id="26UovjNZ40O" role="1m5AlR">
                            <node concept="30H73N" id="26UovjNZ3_1" role="2Oq$k0" />
                            <node concept="3TrEf2" id="26UovjNZ4Hy" role="2OqNvi">
                              <ref role="3Tt5mk" to="av4b:ub9nkyHAbI" resolve="op" />
                            </node>
                          </node>
                        </node>
                        <node concept="3TrcHB" id="26UovjNZ6ML" role="2OqNvi">
                          <ref role="3TsBF5" to="av4b:4kV9Ob9YBYR" resolve="decimals" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjNZ3kt" role="356sEH">
            <property role="TrG5h" value=");" />
          </node>
          <node concept="2EixSi" id="26UovjNZ2GB" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjNZ2GC" role="30HLyM">
        <node concept="3clFbS" id="26UovjNZ2GD" role="2VODD2">
          <node concept="3clFbF" id="26UovjNZ2GE" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjNZ2GF" role="3clFbG">
              <node concept="2OqwBi" id="26UovjNZ2GG" role="2Oq$k0">
                <node concept="30H73N" id="26UovjNZ2GH" role="2Oq$k0" />
                <node concept="3TrEf2" id="26UovjNZ2GI" role="2OqNvi">
                  <ref role="3Tt5mk" to="av4b:ub9nkyHAbI" resolve="op" />
                </node>
              </node>
              <node concept="1mIQ4w" id="26UovjNZ2GJ" role="2OqNvi">
                <node concept="chp4Y" id="26UovjNZ2GK" role="cj9EA">
                  <ref role="cht4Q" to="av4b:4kV9Ob9XpO0" resolve="RealEqualsTestOp" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjO0ydT" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:3kdFyLYhwM6" resolve="AssertOptionTestItem" />
      <node concept="gft3U" id="26UovjO0CBc" role="1lVwrX">
        <node concept="356sEK" id="26UovjO0CBS" role="gfFT$">
          <node concept="356sEF" id="26UovjO0CBT" role="356sEH">
            <property role="TrG5h" value="Assert.True((" />
          </node>
          <node concept="356sEF" id="26UovjO0CC1" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjO0CC6" role="lGtFl">
              <node concept="3NFfHV" id="26UovjO0CC7" role="3NFExx">
                <node concept="3clFbS" id="26UovjO0CC8" role="2VODD2">
                  <node concept="3clFbF" id="26UovjO0CCe" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjO0CC9" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjO0CCc" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:3kdFyLYhwM7" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjO0CCd" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjO0CBY" role="356sEH">
            <property role="TrG5h" value=") is not null);" />
          </node>
          <node concept="2EixSi" id="26UovjO0CBU" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjO0yi8" role="30HLyM">
        <node concept="3clFbS" id="26UovjO0yi9" role="2VODD2">
          <node concept="3clFbF" id="26UovjO0yE2" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjO0CeA" role="3clFbG">
              <node concept="2OqwBi" id="26UovjO0z7L" role="2Oq$k0">
                <node concept="30H73N" id="26UovjO0yE1" role="2Oq$k0" />
                <node concept="3TrcHB" id="26UovjO0C2r" role="2OqNvi">
                  <ref role="3TsBF5" to="av4b:17Nm8oCo8O8" resolve="what" />
                </node>
              </node>
              <node concept="21noJN" id="26UovjO0Cy7" role="2OqNvi">
                <node concept="21nZrQ" id="26UovjO0Cy9" role="21noJM">
                  <ref role="21nZrZ" to="av4b:17Nm8oCo8NZ" resolve="some" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjO0CKh" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:3kdFyLYhwM6" resolve="AssertOptionTestItem" />
      <node concept="gft3U" id="26UovjO0CKi" role="1lVwrX">
        <node concept="356sEK" id="26UovjO0CKj" role="gfFT$">
          <node concept="356sEF" id="26UovjO0CKk" role="356sEH">
            <property role="TrG5h" value="Assert.True((" />
          </node>
          <node concept="356sEF" id="26UovjO0CKl" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjO0CKm" role="lGtFl">
              <node concept="3NFfHV" id="26UovjO0CKn" role="3NFExx">
                <node concept="3clFbS" id="26UovjO0CKo" role="2VODD2">
                  <node concept="3clFbF" id="26UovjO0CKp" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjO0CKq" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjO0CKr" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:3kdFyLYhwM7" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjO0CKs" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjO0CKt" role="356sEH">
            <property role="TrG5h" value=") is null);" />
          </node>
          <node concept="2EixSi" id="26UovjO0CKu" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjO0CKv" role="30HLyM">
        <node concept="3clFbS" id="26UovjO0CKw" role="2VODD2">
          <node concept="3clFbF" id="26UovjO0CKx" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjO0CKy" role="3clFbG">
              <node concept="2OqwBi" id="26UovjO0CKz" role="2Oq$k0">
                <node concept="30H73N" id="26UovjO0CK$" role="2Oq$k0" />
                <node concept="3TrcHB" id="26UovjO0CK_" role="2OqNvi">
                  <ref role="3TsBF5" to="av4b:17Nm8oCo8O8" resolve="what" />
                </node>
              </node>
              <node concept="21noJN" id="26UovjO0CKA" role="2OqNvi">
                <node concept="21nZrQ" id="26UovjO0CKB" role="21noJM">
                  <ref role="21nZrZ" to="av4b:17Nm8oCo8O0" resolve="none" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjO0DgP" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:3kdFyLYhwM6" resolve="AssertOptionTestItem" />
      <node concept="gft3U" id="26UovjO0DgQ" role="1lVwrX">
        <node concept="356sEK" id="26UovjO0DgR" role="gfFT$">
          <node concept="356sEF" id="26UovjO0DgS" role="356sEH">
            <property role="TrG5h" value="Assert.True((" />
          </node>
          <node concept="356sEF" id="26UovjO0DgT" role="356sEH">
            <property role="TrG5h" value="actual" />
            <node concept="29HgVG" id="26UovjO0DgU" role="lGtFl">
              <node concept="3NFfHV" id="26UovjO0DgV" role="3NFExx">
                <node concept="3clFbS" id="26UovjO0DgW" role="2VODD2">
                  <node concept="3clFbF" id="26UovjO0DgX" role="3cqZAp">
                    <node concept="2OqwBi" id="26UovjO0DgY" role="3clFbG">
                      <node concept="3TrEf2" id="26UovjO0DgZ" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:3kdFyLYhwM7" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="26UovjO0Dh0" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="26UovjO0Dh1" role="356sEH">
            <property role="TrG5h" value=") is System.Collections.IEnumerable);" />
          </node>
          <node concept="2EixSi" id="26UovjO0Dh2" role="2EinRH" />
        </node>
      </node>
      <node concept="30G5F_" id="26UovjO0Dh3" role="30HLyM">
        <node concept="3clFbS" id="26UovjO0Dh4" role="2VODD2">
          <node concept="3clFbF" id="26UovjO0Dh5" role="3cqZAp">
            <node concept="2OqwBi" id="26UovjO0Dh6" role="3clFbG">
              <node concept="2OqwBi" id="26UovjO0Dh7" role="2Oq$k0">
                <node concept="30H73N" id="26UovjO0Dh8" role="2Oq$k0" />
                <node concept="3TrcHB" id="26UovjO0Dh9" role="2OqNvi">
                  <ref role="3TsBF5" to="av4b:17Nm8oCo8O8" resolve="what" />
                </node>
              </node>
              <node concept="21noJN" id="26UovjO0Dha" role="2OqNvi">
                <node concept="21nZrQ" id="26UovjO0Dhb" role="21noJM">
                  <ref role="21nZrZ" to="av4b:17Nm8oCo8O1" resolve="coll" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sLUcG" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:5Pgo_AS3Joq" resolve="AssertThatTestItem" />
      <node concept="gft3U" id="7W7a84sLYdP" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sLYeJ" role="gfFT$">
          <node concept="356sEF" id="7W7a84sLYeK" role="356sEH">
            <property role="TrG5h" value="Assert.True(" />
          </node>
          <node concept="356sEF" id="7W7a84sLYeS" role="356sEH">
            <property role="TrG5h" value="match" />
            <node concept="29HgVG" id="7W7a84sLYeX" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sLYeY" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sLYeZ" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sLYf5" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sLYf0" role="3clFbG">
                      <node concept="3TrEf2" id="7W7a84sLYf3" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:5Pgo_AS3PT1" resolve="matcher" />
                      </node>
                      <node concept="30H73N" id="7W7a84sLYf4" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sLYeP" role="356sEH">
            <property role="TrG5h" value=");" />
          </node>
          <node concept="2EixSi" id="7W7a84sLYeL" role="2EinRH" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="7W7a84sNlJb" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:7aipPVpH1LO" resolve="ReportTestItem" />
      <node concept="gft3U" id="7W7a84sNmEL" role="1lVwrX">
        <node concept="356sEK" id="7W7a84sNmER" role="gfFT$">
          <node concept="356sEF" id="7W7a84sNmES" role="356sEH">
            <property role="TrG5h" value="output.WriteLine($&quot;" />
          </node>
          <node concept="356sEK" id="7W7a84sNmX5" role="356sEH">
            <node concept="2EixSi" id="7W7a84sNmX7" role="2EinRH" />
            <node concept="356sEF" id="7W7a84sNn5j" role="356sEH">
              <property role="TrG5h" value="name" />
              <node concept="17Uvod" id="7W7a84sNp6g" role="lGtFl">
                <property role="2qtEX9" value="name" />
                <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                <node concept="3zFVjK" id="7W7a84sNp6h" role="3zH0cK">
                  <node concept="3clFbS" id="7W7a84sNp6i" role="2VODD2">
                    <node concept="3clFbF" id="7W7a84sNpaX" role="3cqZAp">
                      <node concept="2OqwBi" id="7W7a84sNqse" role="3clFbG">
                        <node concept="2OqwBi" id="7W7a84sNpAJ" role="2Oq$k0">
                          <node concept="30H73N" id="7W7a84sNpaW" role="2Oq$k0" />
                          <node concept="3TrEf2" id="7W7a84sNqjt" role="2OqNvi">
                            <ref role="3Tt5mk" to="4kwy:cJpacq40jC" resolve="optionalName" />
                          </node>
                        </node>
                        <node concept="3TrcHB" id="7W7a84sNqOt" role="2OqNvi">
                          <ref role="3TsBF5" to="4kwy:cJpacq408b" resolve="optionalName" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="356sEF" id="7W7a84sNmOd" role="356sEH">
              <property role="TrG5h" value=": " />
            </node>
            <node concept="1W57fq" id="7W7a84sNn7R" role="lGtFl">
              <node concept="3IZrLx" id="7W7a84sNn7S" role="3IZSJc">
                <node concept="3clFbS" id="7W7a84sNn7T" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sNnjO" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sNoEO" role="3clFbG">
                      <node concept="2OqwBi" id="7W7a84sNnNz" role="2Oq$k0">
                        <node concept="30H73N" id="7W7a84sNnjN" role="2Oq$k0" />
                        <node concept="3TrEf2" id="7W7a84sNowh" role="2OqNvi">
                          <ref role="3Tt5mk" to="4kwy:cJpacq40jC" resolve="optionalName" />
                        </node>
                      </node>
                      <node concept="3x8VRR" id="7W7a84sNoPE" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sNmEX" role="356sEH">
            <property role="TrG5h" value="value" />
            <node concept="29HgVG" id="7W7a84sNmF5" role="lGtFl">
              <node concept="3NFfHV" id="7W7a84sNmF6" role="3NFExx">
                <node concept="3clFbS" id="7W7a84sNmF7" role="2VODD2">
                  <node concept="3clFbF" id="7W7a84sNmFd" role="3cqZAp">
                    <node concept="2OqwBi" id="7W7a84sNmF8" role="3clFbG">
                      <node concept="3TrEf2" id="7W7a84sNmFb" role="2OqNvi">
                        <ref role="3Tt5mk" to="av4b:7aipPVpH1LP" resolve="actual" />
                      </node>
                      <node concept="30H73N" id="7W7a84sNmFc" role="2Oq$k0" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="356sEF" id="7W7a84sNmF0" role="356sEH">
            <property role="TrG5h" value="&quot;);" />
          </node>
          <node concept="2EixSi" id="7W7a84sNmET" role="2EinRH" />
        </node>
      </node>
    </node>
  </node>
  <node concept="jVnub" id="26UovjNYErE">
    <property role="TrG5h" value="SwitchTestExpressions" />
    <ref role="phYkn" to="g1u8:1qqzbvY3TCK" resolve="Expression2Expression" />
    <node concept="3aamgX" id="26UovjNYErF" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:6HHp2WmRVXt" resolve="NamedAssertRef" />
      <node concept="gft3U" id="26UovjNYECM" role="1lVwrX">
        <node concept="356sEF" id="26UovjNYECS" role="gfFT$">
          <property role="TrG5h" value="other" />
          <node concept="29HgVG" id="26UovjNYENy" role="lGtFl">
            <node concept="3NFfHV" id="26UovjNYENz" role="3NFExx">
              <node concept="3clFbS" id="26UovjNYEN$" role="2VODD2">
                <node concept="3clFbF" id="26UovjNYENE" role="3cqZAp">
                  <node concept="2OqwBi" id="VGMJHcd9gZ" role="3clFbG">
                    <node concept="1eOMI4" id="VGMJHcd9h0" role="2Oq$k0">
                      <node concept="10QFUN" id="VGMJHcd9h1" role="1eOMHV">
                        <node concept="3Tqbb2" id="VGMJHcd9h2" role="10QFUM">
                          <ref role="ehGHo" to="av4b:ub9nkyHAba" resolve="AssertTestItem" />
                        </node>
                        <node concept="2OqwBi" id="VGMJHcd9h3" role="10QFUP">
                          <node concept="1iwH7S" id="VGMJHcd9h4" role="2Oq$k0" />
                          <node concept="12$id9" id="VGMJHcd9h5" role="2OqNvi">
                            <node concept="2OqwBi" id="VGMJHcd9h6" role="12$y8L">
                              <node concept="30H73N" id="VGMJHcd9h7" role="2Oq$k0" />
                              <node concept="3TrEf2" id="VGMJHcd9h8" role="2OqNvi">
                                <ref role="3Tt5mk" to="av4b:6HHp2WmRVXx" resolve="item" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3TrEf2" id="27xhIwheeD3" role="2OqNvi">
                      <ref role="3Tt5mk" to="av4b:ub9nkyHAbb" resolve="actual" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNYErJ" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:1$1rueeG254" resolve="NoneExpr" />
      <node concept="gft3U" id="26UovjNYECB" role="1lVwrX">
        <node concept="356sEF" id="26UovjNYECH" role="gfFT$">
          <property role="TrG5h" value="null" />
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNYErP" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:1$1rueeDiNV" resolve="OptExpression" />
      <node concept="gft3U" id="26UovjNYEyp" role="1lVwrX">
        <node concept="356sEF" id="26UovjNYEyq" role="gfFT$">
          <property role="TrG5h" value="none" />
          <node concept="29HgVG" id="26UovjNYEyr" role="lGtFl">
            <node concept="3NFfHV" id="26UovjNYEys" role="3NFExx">
              <node concept="3clFbS" id="26UovjNYEyt" role="2VODD2">
                <node concept="3clFbF" id="26UovjNYEyu" role="3cqZAp">
                  <node concept="2OqwBi" id="26UovjNYEyv" role="3clFbG">
                    <node concept="3TrEf2" id="26UovjNYEyw" role="2OqNvi">
                      <ref role="3Tt5mk" to="av4b:1$1rueeDiNY" resolve="expr" />
                    </node>
                    <node concept="30H73N" id="26UovjNYEyx" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3aamgX" id="26UovjNYErX" role="3aUrZf">
      <property role="36QftV" value="true" />
      <ref role="30HIoZ" to="av4b:3GdqffBS$Mm" resolve="MuteEffect" />
      <node concept="gft3U" id="26UovjNYEs7" role="1lVwrX">
        <node concept="356sEF" id="26UovjNYEsd" role="gfFT$">
          <property role="TrG5h" value="none" />
          <node concept="29HgVG" id="26UovjNYEsg" role="lGtFl">
            <node concept="3NFfHV" id="26UovjNYEsh" role="3NFExx">
              <node concept="3clFbS" id="26UovjNYEsi" role="2VODD2">
                <node concept="3clFbF" id="26UovjNYEso" role="3cqZAp">
                  <node concept="2OqwBi" id="26UovjNYEsj" role="3clFbG">
                    <node concept="3TrEf2" id="26UovjNYEsm" role="2OqNvi">
                      <ref role="3Tt5mk" to="av4b:3GdqffBS$Mq" resolve="expr" />
                    </node>
                    <node concept="30H73N" id="26UovjNYEsn" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

